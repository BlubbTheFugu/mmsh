Die Verwaltung der Komponenten des Systems befindet sich in der mmconf.xml Datei
Ausgangsdatenbankschema: dbscheme.sql
Grammatik der CMUSphinx Spracherkennung: res/grammar/dialog.gram

Für den Build wird maven benötigt, dabei erst "mvn clean" und danach "mvn clean install" ausführen.