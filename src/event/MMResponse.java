package event;

import modality.Modality.response;

public class MMResponse {
	private response type;
	private String message;
	
	public MMResponse(response type,String message) {
		this.type = type;
		this.message = message;
	}
	
	public MMResponse(String defaultFailure) {
		this.message = defaultFailure;
		this.type = response.INVALID;
	}

	public response getType() {
		return type;
	}
	public void setType(response type) {
		this.type = type;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
}
