package event;

import java.util.ArrayList;
import event.MMCmdType.cmdType;
import javafx.scene.layout.Pane;

public class MMCmd{
	
	private int modID;
	private String context;
	private cmdType type;
	private Pane destination;
	private ArrayList<?> param;
//	private State response;
	private MMResponse response;	
	public String getContext() {
		return context;
	}

	public cmdType getType() {
		return type;
	}

	public Pane getDestination() {
		return destination;
	}
	
	public int getModID() {
		return modID;
	}

	public MMResponse getResponse() {
		return response;
	}

	public void setResponse(MMResponse response) {
		this.response = response;
	}

	public Object getParam(int position) {
		if(param==null||param.isEmpty())return null;
		return param.get(position);
	}
	
	public MMCmd(int modID, String context, Pane destination, cmdType type, ArrayList<?> param){
		this.modID = modID;
		this.context = context;
		this.destination = destination;
		this.type = type;
		this.param = param;
		
	}
}
