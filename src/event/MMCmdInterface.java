
package event;

import java.util.ArrayList;

import util.Song;
import modality.Modality;
import modality.Modality.response;

public interface MMCmdInterface {

	default MMResponse selectRoom(String roomName){
		return new MMResponse("Could not select room "+roomName);
	}
	default MMResponse addRoom(String roomName){
		return new MMResponse("Could not add room "+roomName);
	}
	default MMResponse deleteRoom(String roomName){
		return new MMResponse("Could not delete room "+roomName);
	}
	default MMResponse changeTemperature(String roomName,int tmp){
		return new MMResponse("Could not change temperature of "+roomName+" to "+tmp+" degrees");
	}
	default MMResponse switchLight(){	
		return new MMResponse("Could not switch light");
	}
	default MMResponse addPlaylist(String playlistName){
		return new MMResponse("Could not add playlist "+playlistName);
	}
	default MMResponse deletePlaylist(String playlistName){
		return new MMResponse("Could not delete playlist "+playlistName);
	}
	default MMResponse addSongs(ArrayList<Song> s){
		return new MMResponse("Could not add songs");
	}
	default MMResponse turnSocketOn(int socketID){
		return new MMResponse("Could not turn socket "+socketID+" on");
	}
	default MMResponse turnSocketOff(int socketID){
		return new MMResponse("Could not turn socket "+socketID+" off");
	}
	default MMResponse play(){
		return new MMResponse("Could not play");
	}
	default MMResponse pause(){
		return new MMResponse("Could not pause playback");
	}
	default MMResponse stop(){
		return new MMResponse("Could not stop playback");
	}
	default MMResponse next(){
		return new MMResponse("Could not play next track");
	}
	default MMResponse previous(){
		return new MMResponse("Could not play previous track");
	}
	default MMResponse setMute(boolean mute){
		return new MMResponse("Could not set mute "+mute);
	}
	default MMResponse setRandom(boolean random){
		return new MMResponse("Could not set random "+random);
	}
	default MMResponse forwardPlayback(int value){
		return new MMResponse("Could not forward playback");
	}
	default MMResponse backwardPlayback(int value){
		return new MMResponse("Could not backward playback");
	}
	default MMResponse newLayout(){
		return new MMResponse("Could not delete roomlayout");
	}
	default MMResponse setVolume(double volume){
		return new MMResponse("Could not set volume to "+(int)(volume*100)+" percent");
	}
	default MMResponse updateView(){
		return new MMResponse("View was switched");
	}
	default MMResponse raiseVolume(double volume){
		return new MMResponse("Could not raise volume");
	}
	default MMResponse lowerVolume(double volume){
		return new MMResponse("Could not lower volume");
	}
	default MMResponse switchGarage(boolean garage){
		return new MMResponse("Could not change garage state");
	}
	default MMResponse switchSurveilance(boolean surveilance){
		return new MMResponse("Could not change camera state");
	}
	default MMResponse switchIntrusionDetection(boolean intrusionDetection){
		return new MMResponse("Could not change intrusion detection state");
	}
	default MMResponse switchSmokeAlarm(boolean smokeAlarm){
		return new MMResponse("Could not change fire alarm state");
	}
	default MMResponse changeBlindState(String roomName,boolean state){
		return new MMResponse("Could not change blind state");
	}
	default MMResponse changeLightState(String roomName,boolean state){
		return new MMResponse("Could not change light state");
	}
}
