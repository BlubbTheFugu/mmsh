package main;

import integrated.visual.OutputBox;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.Map;

import modality.SimpleOutputModality;
import util.DialogHandler;

import com.guigarage.flatterfx.FlatterFX;

import de.jensd.fx.glyphs.GlyphsDude;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIcon;
import event.MMCmd;
import event.MMCmdType.cmdType;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckMenuItem;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.RadioMenuItem;
import javafx.scene.control.SeparatorMenuItem;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class GUI extends Application implements DialogHandler{
	private Control control;

	private static final int startScreenWidth=800;
	private static final int startScreenHeight=600;
	public static final int modID = -1;
	private Stage primaryStage;
	private Pane activeView;
	private BorderPane root;
	private OutputBox output;
	private Menu inputMenu,outputMenu;
	private MenuBar menuBar;

	public void setActiveView(Pane p){
		root.setCenter(p);
		activeView = p;
	}
	
	public Pane getActiveView() {
		return activeView;
	}

	@Override
	public void start(Stage startStage) throws Exception {
		//loadIcons();
		this.primaryStage = startStage;
		primaryStage.setTitle("Multimodale Haussteuerung");
		primaryStage.setMinHeight(600);
		primaryStage.setMinWidth(800);
		primaryStage.setMaxWidth(1280);
		primaryStage.setMaxHeight(1024);
		FlatterFX.style();
		root = new BorderPane();
		root.setPadding(new Insets(5,10,10,10));
		menuBar = buildMenu();
		root.setTop(menuBar);
		root.setLeft(buildSidebar());		
		
//		sidebar.getStylesheets().add("sidebar.css");
		Scene start = new Scene(root,startScreenWidth,startScreenHeight);
		//start.getStylesheets().add(getClass().getResource("/css/font-awesome.css").toExternalForm());
		primaryStage.setScene(start);
		control = new Control(this);
		updateIOEntries();
		setActiveView(control.getView("OverView"));
		primaryStage.show();
	}

	private MenuBar buildMenu(){
		MenuBar menuBar= new MenuBar();
	    menuBar.prefWidthProperty().bind(primaryStage.widthProperty());
	    // File menu - new, save, exit
//	    Menu fileMenu = new Menu("File");
//	    MenuItem newMenuItem = new MenuItem("New");
//	    MenuItem saveMenuItem = new MenuItem("Save");
//	    MenuItem exitMenuItem = new MenuItem("Exit");
//	    exitMenuItem.setOnAction(actionEvent -> Platform.exit());
	    
//	    fileMenu.getItems().addAll(newMenuItem, saveMenuItem,
//	        new SeparatorMenuItem(), exitMenuItem);

	    inputMenu = new Menu("Input");
	    outputMenu = new Menu("Output");
	    
	    menuBar.getMenus().addAll( inputMenu, outputMenu);
	    return menuBar;	    
	}
	private CheckMenuItem createCheckMenuItem(int modID,String name, boolean activated,boolean input){
		CheckMenuItem menuItem = new CheckMenuItem(name);
		menuItem.setSelected(activated);
		menuItem.selectedProperty().addListener(new ChangeListener<Boolean>() {
            public void changed(ObservableValue ov,
                    Boolean old_val, Boolean new_val) {
            	if(input){
            		if(new_val)GUI.this.control.addInputEvent(cmdType.ACTIVATE_INPUT, new ArrayList<>(Arrays.asList(name,modID)));
            		else GUI.this.control.addInputEvent(cmdType.DEACTIVATE_INPUT, new ArrayList<>(Arrays.asList(name,modID)));           		
            	}else{
            		if(new_val)GUI.this.control.addInputEvent(cmdType.ACTIVATE_OUTPUT, new ArrayList<>(Arrays.asList(name,modID)));
            		else GUI.this.control.addInputEvent(cmdType.DEACTIVATE_OUTPUT, new ArrayList<>(Arrays.asList(name,modID)));           		   		
            	}
            }
        });
        return menuItem;
	}
	
	public void updateIOEntries(){
		menuBar.getMenus().remove(inputMenu);
		menuBar.getMenus().remove(outputMenu);
		inputMenu = new Menu("Input");
		outputMenu = new Menu("Output");
		Iterator in = control.getInputNames().entrySet().iterator();
		int i = 0;
	    while (in.hasNext()) {
	        Map.Entry<String,Boolean> pair = (Map.Entry<String,Boolean>)in.next();
			inputMenu.getItems().add(createCheckMenuItem(i,pair.getKey(), pair.getValue(), true));
			i++;
	    }
	    Iterator out = control.getOutputNames().entrySet().iterator();
	    i = 0;
	    while (out.hasNext()) {
	    	Map.Entry<String,Boolean> pair = (Map.Entry<String,Boolean>)out.next();
	    	outputMenu.getItems().add(createCheckMenuItem(i,pair.getKey(), pair.getValue(), false));
//	    	if(pair.getKey()=="OutputBox")root.setBottom(control);
	    	i++;
	    }
		menuBar.getMenus().addAll(inputMenu,outputMenu);
	}
	
	private VBox buildSidebar(){
		VBox sidebar = new VBox();
		sidebar.setId("sidebar");
		sidebar.getStylesheets().add("sidebar.css");
		Button overview = GlyphsDude.createIconButton(FontAwesomeIcon.MAP,"Overview","15px;","13px",ContentDisplay.LEFT);
		overview.setOnAction((event) -> {
		   genSwitchEvent("OverView");
		});
		Button power = GlyphsDude.createIconButton(FontAwesomeIcon.PLUG,"Power","15px;","13px",ContentDisplay.LEFT);
		power.setOnAction((event) -> {
			   genSwitchEvent("PowerView");
		});
		Button entertainment = GlyphsDude.createIconButton(FontAwesomeIcon.TELEVISION,"Entertainment","15px;","13px",ContentDisplay.LEFT);
		entertainment.setOnAction((event) -> {
			   genSwitchEvent("EntertainmentView");
		});
		
		Button security = GlyphsDude.createIconButton(FontAwesomeIcon.LOCK,"Security","15px;","13px",ContentDisplay.LEFT);
		security.setOnAction((event) -> {
			   genSwitchEvent("SecurityView");
		});
		
		Button log = GlyphsDude.createIconButton(FontAwesomeIcon.LIST_ALT,"Event Log","15px;","13px",ContentDisplay.LEFT);
		log.setOnAction((event) -> {
			   genSwitchEvent("EventView");
		});
		Button music = GlyphsDude.createIconButton(FontAwesomeIcon.MUSIC,"Music");
		music.setOnAction((event) -> {
			   genSwitchEvent("music");
		});

		//TODO: Undecorated CSS Buttons or even buttons
		//		music.setMinWidth(120);
		sidebar.getChildren().addAll(overview,power,entertainment,security,log);
	    return sidebar;
	}
	
	private void genSwitchEvent(String viewName){
		control.addInputEvent(cmdType.SWITCH_VIEW,new ArrayList<>(Arrays.asList(viewName)));
	}
		
	public static void main(String[]args){
		launch(args);
	}

	public void addOutputBox(SimpleOutputModality sout) {
		output = (OutputBox) sout;
		root.setBottom(output.getOutputPane());
	}
}
