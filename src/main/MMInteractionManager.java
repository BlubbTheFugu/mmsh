package main;

import integrated.audio.JSGFGrammarUpdater;

import java.util.ArrayList;
import java.util.LinkedList;

import util.DialogHandler;
import util.Loggable;
import util.LoggedEvent;
import views.EntertainmentView;
import views.OverView;
import views.SecurityView;
import config.MMConfigurator;
import edu.cmu.sphinx.jsgf.JSGFGrammar;
import event.*;
import event.MMCmdType.cmdType;
import javafx.application.Platform;
import javafx.scene.layout.Pane;
import modality.Modality;
import modality.SimpleOutputModality;
import modality.Modality.response;
import modality.Modality.state;
import models.*;

public class MMInteractionManager implements Runnable,Loggable{
	private GUI gui;
	private Control control;
	private Pane viewContext;
	private MMConfigurator mmconfig;
	
	private LinkedList <MMCmd> eventQueue;
	private LinkedList <MMCmd> eventHistory;
	private EventModel logger = EventModel.getInstance();
	
	public MMInteractionManager(Control control, GUI gui){
		this.gui = gui;
		this.control = control;
		eventQueue = new  LinkedList<MMCmd>();
		eventHistory = new  LinkedList<MMCmd>();
		viewContext = control.getView(OverView.class.getSimpleName());
		mmconfig = new MMConfigurator(this);
		for(SimpleOutputModality sout: mmconfig.getOutputDevices()){
			if(sout.getAttribute().getClassName().equals("OutputBox")){
			    getLogger().info("Added outputbox");
			    gui.addOutputBox(sout);
			}
		}
		Thread commandHandler = new Thread(this);
		commandHandler.setDaemon(true);
		commandHandler.start();
	}
	
	private MMCmd computeInput(MMCmd cmd){
		viewContext = cmd.getDestination();
		MMCmdInterface view = null;
		if(cmd.getDestination()!=null)view = (MMCmdInterface) cmd.getDestination();
		MMResponse re = null;
    	switch(cmd.getType()){
    	case ACTIVATE_INPUT:
    		mmconfig.activateInput((String) cmd.getParam(0),(int)cmd.getParam(1));
    		re = new MMResponse("Input "+cmd.getParam(0)+" was activated");break;
    	case DEACTIVATE_INPUT:
    		mmconfig.deativateInput((String) cmd.getParam(0),(int)cmd.getParam(1));
    		re = new MMResponse("Input "+cmd.getParam(0)+" was deactivated");break;
    	case ACTIVATE_OUTPUT:
    		mmconfig.activateOutput((String) cmd.getParam(0),(int)cmd.getParam(1));
    		re = new MMResponse("Output "+cmd.getParam(0)+" was activated");break;
    	case DEACTIVATE_OUTPUT:
    		mmconfig.deativateOutput((String) cmd.getParam(0),(int)cmd.getParam(1));
    		re = new MMResponse("Output "+cmd.getParam(0)+" was deactivated");break;
    	case SWITCH_VIEW:
    		viewContext = control.getView((String)cmd.getParam(0));
    		gui.setActiveView(control.getView((String)cmd.getParam(0)));
    		re = view.updateView();
    		break;
    	case SWITCH_ROOM:
    		re = view.selectRoom((String)cmd.getParam(0));
    		break;
    	case CHANGE_TEMP:
    		TemperatureModel.getInstance().changeTemperature((String)cmd.getParam(0), (int)cmd.getParam(1));
    		re = view.changeTemperature((String)cmd.getParam(0), (int)cmd.getParam(1));
    		break;
    	case ADD_ROOM:
    		RoomModel.getInstance().addRoom((String)cmd.getParam(0));
    		int roomID = RoomModel.getInstance().getRoomNames().size()-1;
    		LightModel.getInstance().addLightsource(roomID,(String)cmd.getParam(0), "Overhead Lamp");
    		TemperatureModel.getInstance().update();
    		BlindsModel.getInstance().update();
    		re = view.addRoom((String)cmd.getParam(0));
    		JSGFGrammarUpdater.updateRoomNames(RoomModel.getInstance().getRoomNames());
    		break;
    	case DELETE_ROOM:
    		RoomModel.getInstance().deleteRoom((String)cmd.getParam(0));
    		TemperatureModel.getInstance().update();
    		LightModel.getInstance().update();
    		re = view.deleteRoom((String)cmd.getParam(0));
    		break;
    	case NEW_LAYOUT:
    		RoomModel.getInstance().newLayout();
    		re = view.newLayout();
    		break;
		case ADD_PLAYLIST:
			String playlistname = "New playlist";
			if(cmd.getParam(0)==null){
				playlistname = gui.textInput(playlistname, "Add Playlist", "Choose a name for your new playlist", "Playlist name:");
			}else playlistname= (String) cmd.getParam(0);
			if(playlistname!=null){
				re = view.addPlaylist(playlistname);
				EntertainmentModel.getInstance().addPlaylist(playlistname);
			}
			break;
		case DELETE_PLAYLIST:
			if(cmd.getParam(0)==null){
				ArrayList<String> names = EntertainmentModel.getInstance().getPlaylistNames();
				playlistname = gui.choiceInput(names, "Delete playlist", "Which playlist do you want to delete?", "Playlist name:");
			}else playlistname= (String) cmd.getParam(0);
			if(playlistname!=null){
				re = view.deletePlaylist(playlistname);
				EntertainmentModel.getInstance().deletePlaylist(playlistname);
			}
			break;
		case TURN_SOCKET_ON:
			if(cmd.getParam(0)!=null){
				int socketID = (int) cmd.getParam(0);
				re = view.turnSocketOn(socketID);
				PowerModel.getInstance().turnSocketOn(socketID);
			}			
			break;
		case TURN_SOCKET_OFF:
			if(cmd.getParam(0)!=null){
				int socketID = (int) cmd.getParam(0);
				re = view.turnSocketOff(socketID);
				PowerModel.getInstance().turnSocketOff(socketID);
			}			
			break;
		case PLAY:
			re = ((MMCmdInterface) control.getView(EntertainmentView.class.getSimpleName())).play();
			break;
		case PAUSE:
			re = ((MMCmdInterface) control.getView(EntertainmentView.class.getSimpleName())).pause();
			break;
		case STOP:
			re = ((MMCmdInterface) control.getView(EntertainmentView.class.getSimpleName())).stop();
			break;
		case NEXT:
			re = ((MMCmdInterface) control.getView(EntertainmentView.class.getSimpleName())).next();
			break;
		case PREVIOUS:
			re = ((MMCmdInterface) control.getView(EntertainmentView.class.getSimpleName())).previous();
			break;
		case MUTE_ON:
			re = ((MMCmdInterface) control.getView(EntertainmentView.class.getSimpleName())).setMute(true);
			break;
		case MUTE_OFF:
			re = ((MMCmdInterface) control.getView(EntertainmentView.class.getSimpleName())).setMute(false);
			break;
		case RANDOM_ON:
			re = ((MMCmdInterface) control.getView(EntertainmentView.class.getSimpleName())).setRandom(true);
			break;
		case RANDOM_OFF:
			re = ((MMCmdInterface) control.getView(EntertainmentView.class.getSimpleName())).setRandom(false);
			break;
		case SET_VOLUME:
			re = ((MMCmdInterface) control.getView(EntertainmentView.class.getSimpleName())).setVolume((double)cmd.getParam(0));
			break;
		case RAISE_VOLUME:
			re = ((MMCmdInterface) control.getView(EntertainmentView.class.getSimpleName())).raiseVolume((double)cmd.getParam(0));
			break;
		case LOWER_VOLUME:
			re = ((MMCmdInterface) control.getView(EntertainmentView.class.getSimpleName())).lowerVolume((double)cmd.getParam(0));
			break;
		case BACKWARDS:
			re = ((MMCmdInterface) control.getView(EntertainmentView.class.getSimpleName())).backwardPlayback((int) cmd.getParam(0));
			break;
		case FORWARDS:
			re = ((MMCmdInterface) control.getView(EntertainmentView.class.getSimpleName())).forwardPlayback((int) cmd.getParam(0));
			break;
		case SWITCH_GARAGE:
			re = ((MMCmdInterface) control.getView(SecurityView.class.getSimpleName())).switchGarage((boolean)cmd.getParam(0));
			SecurityModel.getInstance().setGarage((boolean)cmd.getParam(0));
			break;
		case SWITCH_SURVEILANCE:
			re = ((MMCmdInterface) control.getView(SecurityView.class.getSimpleName())).switchSurveilance((boolean)cmd.getParam(0));
			SecurityModel.getInstance().setSurveilance((boolean)cmd.getParam(0));
			break;
		case SWITCH_INTRUSION:
			re = ((MMCmdInterface) control.getView(SecurityView.class.getSimpleName())).switchIntrusionDetection((boolean)cmd.getParam(0));
			SecurityModel.getInstance().setIntrusionAlarm((boolean)cmd.getParam(0));
			break;
		case SWITCH_SMOKE_ALARM:
			re = ((MMCmdInterface) control.getView(SecurityView.class.getSimpleName())).switchSmokeAlarm((boolean)cmd.getParam(0));
			SecurityModel.getInstance().setSmokeAlarm((boolean)cmd.getParam(0));
			break;
		case CHANGE_BLIND_STATE:
			re = ((MMCmdInterface) control.getView(OverView.class.getSimpleName())).changeBlindState((String)cmd.getParam(0),(boolean)cmd.getParam(1));
			BlindsModel.getInstance().changeBlindsState((String)cmd.getParam(0),(boolean)cmd.getParam(1));
			break;
		case CHANGE_LIGHT_STATE:
		    re = ((MMCmdInterface) control.getView(OverView.class.getSimpleName())).changeLightState((String)cmd.getParam(0),(boolean)cmd.getParam(1));
            LightModel.getInstance().changeLightState((String)cmd.getParam(0),(boolean)cmd.getParam(1));
            break;
//		case SWITCH_ROOM_LIGHT:
//			re = ((MMCmdInterface) control.getView(OverView.class.getSimpleName())).changeLightState((boolean)cmd.getParam(1));
//			LightModel.getInstance().changeLightState((String)cmd.getParam(0),(boolean)cmd.getParam(1));
//			break;
		default:
			re = new MMResponse(cmd.getType()+" not handled");
			getLogger().info("CmdType: "+cmd.getType()+" not handled");
			break;
    	}
    	cmd.setResponse(re);   
    	return cmd;
	}
	
	private void computeOutput(MMCmd cmd){
		MMResponse re = cmd.getResponse();
		for(SimpleOutputModality om:mmconfig.getOutputDevices()){
		    if(om.getCurrentState()!=state.PAUSED){
				om.output(cmd);					
				getLogger().info(om.getAttribute().getClassName()+" "+om.getCurrentState().toString());
			}
		}
		logEvent(cmd);
	}

	@Override
	public void run() {
		while(true){
			if(!eventQueue.isEmpty()){
				Platform.runLater(new Runnable() {
					@Override
					public void run() {
						MMCmd event = eventQueue.pollFirst();
						MMCmd responseCmd = computeInput(event);
						computeOutput(responseCmd);		
						eventHistory.add(responseCmd);						
					}
				});
			}
			if(eventHistory.size()>50)eventHistory.remove(eventHistory.getLast());
			try {
				Thread.sleep(50);
			} catch (InterruptedException e) {
				getLogger().error("Thread not running");
				e.printStackTrace();
			}
		}
	}
	
	public void addEvent(MMCmd event){
		eventQueue.add(event);
	}
	
	private void logEvent(MMCmd event){
		String destination="Not specified";
		if(event.getDestination()!=null)destination = event.getDestination().getClass().getCanonicalName();
		String type = event.getType().name();
		String context = event.getContext();
		String mod = "GUI";
		String state = "state.Running";
		String responseMessage = "No response";
		if(event.getModID()>-1){
			state = mmconfig.getInputDevices().get(event.getModID()).getCurrentState().name();
			mod = mmconfig.getInputDevices().get(event.getModID()).getClass().getName();
		}		
		responseMessage = event.getResponse().getMessage();
		LoggedEvent le = new LoggedEvent(type,destination,context,mod,state,responseMessage);
		logger.addEvent(le);
	}
	
	public ArrayList<Modality> getInputModalities(){
		return mmconfig.getInputDevices();
	}
	
	public ArrayList<SimpleOutputModality> getOutputModalities(){
		return (ArrayList<SimpleOutputModality>)mmconfig.getOutputDevices();
	}
	
	private void updateContext(){
		
	}
	//String rec = mmconfig.getSpeech().recognize();
	//if(rec.startsWith("show")||rec.startsWith("open")){
	//	computeInput(new MMCmd("Speech", gui.getActiveView(), control.getView(rec.split(" ")[1]), MMCmdType.SWITCH_VIEW));
	//}

	public Pane getViewContext() {
		return viewContext;
	}
	
}
