package main;

import java.util.ArrayList;
import java.util.HashMap;

import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import views.EventView;
import views.PowerView;
import views.SecurityView;
import views.OverView;
import views.EntertainmentView;
import event.MMCmd;
import event.MMCmdType.cmdType;
import modality.Modality;
import modality.SimpleOutputModality;
import models.EventModel;
import models.LightModel;
import models.BlindsModel;
import models.EntertainmentModel;
import models.PowerModel;
import models.RoomModel;
import models.TemperatureModel;

public class Control {
	public enum model{
		ROOM,
		LIGHT,
		TEMPERATURE,
		ENTERTAINMENT,
		POWER, EVENT_LOG, BLINDS
	}
	private GUI gui;
	private MMInteractionManager mmi;

	private HashMap<model,ModelInterface> models;
	private HashMap<String,Pane> views;
	
	public Control(GUI gui){
		this.gui = gui;
		initModels();
		initViews();
		this.mmi = new MMInteractionManager(this, gui);
	}
	
	private void initModels(){
		models = new HashMap<model,ModelInterface>();
		RoomModel rooms = RoomModel.getInstance();
		models.put(model.ROOM, rooms);
		PowerModel power = PowerModel.getInstance();
		models.put(model.POWER, power);
		LightModel lights = LightModel.getInstance();
		models.put(model.LIGHT, lights);
		TemperatureModel temp = TemperatureModel.getInstance();
		models.put(model.TEMPERATURE, temp);
		EntertainmentModel entertainment = EntertainmentModel.getInstance();
		models.put(model.ENTERTAINMENT, entertainment);
		EventModel event = EventModel.getInstance();
		models.put(model.EVENT_LOG, event);
		BlindsModel blinds = BlindsModel.getInstance();
		models.put(model.BLINDS, blinds);
	}
	
	private void initViews(){
		views = new HashMap<String,Pane>();
		OverView start = new OverView(this,Color.WHEAT);
		views.put(OverView.class.getSimpleName(),(Pane) start);
		views.put(PowerView.class.getSimpleName(), (Pane)new PowerView(this,Color.AQUAMARINE));
		views.put(EntertainmentView.class.getSimpleName(),(Pane) new EntertainmentView(this,Color.RED));
		views.put(SecurityView.class.getSimpleName(),(Pane) new SecurityView(this,Color.ANTIQUEWHITE));
		views.put(EventView.class.getSimpleName(),(Pane) new EventView(this,Color.ANTIQUEWHITE));
	}
	
	public HashMap<String,Boolean> getInputNames(){
		HashMap<String,Boolean> names = new HashMap<String,Boolean>();
		ArrayList<Modality> mods = mmi.getInputModalities();
		for(int i = 0;i<mods.size();i++){
			names.put(mods.get(i).getAttribute().getClassName(),mods.get(i).getAttribute().isActive());
		}
		return names;
	}
	public HashMap<String,Boolean> getOutputNames(){
		HashMap<String,Boolean> names = new HashMap<String,Boolean>();
		ArrayList<SimpleOutputModality> mods = mmi.getOutputModalities();
		for(int i = 0;i<mods.size();i++){
			names.put(mods.get(i).getAttribute().getClassName(),mods.get(i).getAttribute().isActive());
		}
		return names;
	}
	
	public ModelInterface getModel(model name){
		return models.get(name);
	}
	
	public Pane getView(String viewName){
		return views.get(viewName);
	}
	
	public void addInputEvent(cmdType type, ArrayList<Object> param){
		MMCmd cmd = new MMCmd(GUI.modID, "GUI", gui.getActiveView(), type, param);
		mmi.addEvent(cmd);
	}	
}
