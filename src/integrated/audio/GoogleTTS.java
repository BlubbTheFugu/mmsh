package integrated.audio;

import java.io.IOException;
import java.io.InputStream;

import com.gtranslate.Audio;
import com.gtranslate.Language;
import com.gtranslate.Translator;

import event.MMCmd;
import javazoom.jl.decoder.JavaLayerException;
import main.MMInteractionManager;
import modality.Modality.response;
import modality.Modality.state;
import modality.ModalityAttrib;
import modality.ThreadedOutputModality;

public class GoogleTTS extends ThreadedOutputModality{
	
	private Translator translate;
	private Audio audio;
	
	public GoogleTTS(int modID, ModalityAttrib attribute, MMInteractionManager mmi) {
		super(modID,attribute,mmi);
	}
	
	@Override
	public response prepare() {
		translate = Translator.getInstance();
		Audio audio = Audio.getInstance();
		return response.SUCCESS;
	}

	@Override
	public response cancel() {
		translate = null;
		
		return response.SUCCESS;
	}

	@Override
	protected void executeOutput(MMCmd event) {
		while(outputQueue.size()>5)outputQueue.pollFirst();
		InputStream sound;
		Translator translate = Translator.getInstance();
		Audio audio = Audio.getInstance();
		try {
			sound = audio.getAudio(event.getResponse().getMessage(), Language.ENGLISH);
			audio.play(sound);
		} catch (IOException | JavaLayerException e) {
			getLogger().error("Couldn't play GoogleTTS audiostream");
			e.printStackTrace();
		}
    	
	}
}
