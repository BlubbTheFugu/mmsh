package integrated.audio;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.LogManager;
import java.util.logging.Logger;

import org.apache.commons.lang.WordUtils;

import javafx.scene.layout.Pane;
import util.Loggable;
import views.EntertainmentView;
import views.EventView;
import views.OverView;
import views.PowerView;
import views.SecurityView;
import main.MMInteractionManager;
import modality.Modality;
import modality.Modality.response;
import modality.ModalityAttrib;
import modality.ThreadedInputModality;
import models.BlindsModel;
import models.LightModel;
import models.RoomModel;
import edu.cmu.sphinx.api.Configuration;
import edu.cmu.sphinx.api.LiveSpeechRecognizer;
import event.MMCmd;
import event.MMCmdInterface;
import event.MMCmdType.cmdType;

public class CMUSphinx extends ThreadedInputModality{
	
//	Load grammar in xml
//  config.setGrammarName("digits.grxml");
//  LiveSpeechRecognizer grxmlRecognizer = new LiveSpeechRecognizer(config);	
	
    public CMUSphinx(int modID, ModalityAttrib attribute,MMInteractionManager mmi) {
		super(modID, attribute, mmi);
	}
    private ArrayList<String> dec = new ArrayList<String>(Arrays.asList("zero","ten","twenty","thirty","fourty","fifty","sixty","seventy","eighty","ninety","hundred")); 
	private static final String ACOUSTIC_MODEL = "resource:/edu/cmu/sphinx/models/en-us/en-us";
    private static final String DICTIONARY_PATH = "resource:/edu/cmu/sphinx/models/en-us/cmudict-en-us.dict";
    private static final String GRAMMAR_PATH = "resource:/grammar/";

    private Configuration config;
    private LiveSpeechRecognizer sphinxRecognizer;
   
	@Override
	public response prepare() {
//		getLogger().info("Prepare CMU Sphinx Begin");
		try {
			JSGFGrammarUpdater.updateRoomNames(RoomModel.getInstance().getRoomNames());
			config = new Configuration();
	    	config.setAcousticModelPath(ACOUSTIC_MODEL);
			config.setDictionaryPath(DICTIONARY_PATH);
			config.setGrammarPath(GRAMMAR_PATH);
			config.setUseGrammar(true);
			config.setGrammarName("dialog");
 			sphinxRecognizer = new LiveSpeechRecognizer(config);
 			
 		} catch (Exception e) {
 			getLogger().error("Couldn't initialize Sphinx - LiveSpeechRecognizer");
 			e.printStackTrace();
 			return response.INVALID;
 		}
 		getLogger().info("Prepare CMU Sphinx End");
 		return response.SUCCESS;
	}

	@Override
	public void startThread() {
		sphinxRecognizer.startRecognition(true);
		super.startThread();
	}

	@Override
	protected void threadedMethod() {
		cmdType type = null;
		Pane pane = mmi.getViewContext();
		String view =  mmi.getViewContext().getClass().getSimpleName();
		String utterance = sphinxRecognizer.getResult().getHypothesis();
		ArrayList<Object> param = new ArrayList<>();
		if(utterance!=null&&!utterance.equals("null")&&!utterance.equals("<unk>")){
            MMCmd event = null;
            switch(utterance){
            case "continue playback":type = cmdType.PLAY;break;
            case "pause playback":type = cmdType.PAUSE;break;
            case "stop playback":type = cmdType.STOP;break;
            case "next track":type = cmdType.NEXT;break;
            case "previous track":type = cmdType.PREVIOUS;break;
            case "turn mute on":type = cmdType.MUTE_ON;break;
            case "turn mute off":type = cmdType.MUTE_OFF;break;
            case "turn random on":type = cmdType.RANDOM_ON;break;
            case "turn random off":type = cmdType.RANDOM_OFF;break;
           
            case "toggle light":type = cmdType.TOGGLE_LIGHT;break;
            case "toggle blinds":type = cmdType.TOGGLE_BLINDS;break;     
            
            case "open garage":type = cmdType.SWITCH_GARAGE; param.add(true);break;
            case "close garage":type = cmdType.SWITCH_GARAGE; param.add(false);break;
            case "turn surveillance on":type = cmdType.SWITCH_SURVEILANCE; param.add(true);break;
            case "turn surveillance off":type = cmdType.SWITCH_SURVEILANCE; param.add(false);break;
            case "turn smoke alarm on":type = cmdType.SWITCH_SMOKE_ALARM; param.add(true);break;
            case "turn smoke alarm off":type = cmdType.SWITCH_SMOKE_ALARM; param.add(false);break;
            case "turn intrusion detection on":type = cmdType.SWITCH_INTRUSION; param.add(true);break;
            case "turn intrusion detection off":type = cmdType.SWITCH_INTRUSION; param.add(false);break;
            } 
            if(utterance.startsWith("switch to")||utterance.startsWith("change to")){
            	type = cmdType.SWITCH_VIEW;
            	switch(utterance.split(" ")[2]) {
            	case "over":param.add(OverView.class.getSimpleName());break;
            	case "entertainment":param.add(EntertainmentView.class.getSimpleName());break;
            	case "event":param.add(EventView.class.getSimpleName());break;
            	case "power":param.add(PowerView.class.getSimpleName());break;
            	case "security":param.add(SecurityView.class.getSimpleName());break;
            	default:type = null;
            	}
            }else if(utterance.startsWith("set volume to")&&utterance.endsWith("percent")){
            	String percentageString = utterance.split(" ")[3];
            	int i=0;
            	for(String s:dec){
            		if(s.equals(percentageString)){
            			type = cmdType.SET_VOLUME;
            			param.add(i*0.1d);
            		}
            		i++;
            	}
            }else if(utterance.startsWith("jump")&&utterance.contains("seconds")) {
            	String percentageString = utterance.split(" ")[1];
            	int i=0;
            	for(String s:dec){
            		if(s.equals(percentageString)){
            			if(utterance.contains("backward")) {
            				type = cmdType.BACKWARDS;
            				param.add(i*10);
            			}else if(utterance.contains("forward")) {
            				type = cmdType.FORWARDS;
            				param.add(i*10);
            			}
            		}
            		i++;
            	}
            }else if(utterance.startsWith("select room ")) {
            	String roomName = utterance.split(" ")[2];
                roomName = WordUtils.capitalize(roomName);
            	param.add(roomName);
            	type = cmdType.SWITCH_ROOM;
            }else if(utterance.startsWith("turn light")){
                String roomName = utterance.split(" ")[4];
                roomName = WordUtils.capitalize(roomName);
                param.add(roomName);
                boolean activeState=false;
                String state = utterance.split(" ")[2];
                if(state.equals("on"))activeState=true;
                else activeState=false;
                param.add(activeState);          
            }else if(utterance.contains("blinds in room")){
                String roomName = utterance.split(" ")[4];
                roomName = WordUtils.capitalize(roomName);
                param.add(roomName);
                boolean activeState=false;
                String state = utterance.split(" ")[0];
                if(state.equals("open"))activeState=true;
                else activeState=false;
                param.add(activeState);
               
            }
        	if(type!=null)mmi.addEvent(new MMCmd(modID,utterance,pane,type,param));
		}	
	}

	@Override
	public response pause() {
		setCurrentState(state.PAUSED);
		sphinxRecognizer.stopRecognition();
		return response.SUCCESS;
	}

	@Override
	public response resume() {
		setCurrentState(state.IDLE);
		sphinxRecognizer.startRecognition(true);
		return response.SUCCESS;
	}

	@Override
	public response cancel() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public response start() {
		setCurrentState(state.IDLE);
		sphinxRecognizer.startRecognition(true);
		return response.SUCCESS;
	}
}
