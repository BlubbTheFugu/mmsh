package integrated.audio;

import com.sun.speech.freetts.Voice;
import com.sun.speech.freetts.VoiceManager;

import event.MMCmd;
import main.MMInteractionManager;
import modality.ModalityAttrib;
import modality.ThreadedOutputModality;

public class FreeTTS extends ThreadedOutputModality{
	
	public FreeTTS(int modID, ModalityAttrib attribute, MMInteractionManager mmi) {
		super(modID,attribute,mmi);
	}

	private VoiceManager voiceManager;
	private Voice voice;
	
	@Override
	public response prepare() {
    	VoiceManager vm = VoiceManager.getInstance();
    	System.setProperty("freetts.voices", "com.sun.speech.freetts.en.us.cmu_us_kal.KevinVoiceDirectory");
        voice = vm.getVoice("kevin16");
        voice.allocate();
        if(voice==null||!voice.isLoaded()){
        	return response.INVALID;
        }
		return response.SUCCESS;
	}

	@Override
	public response cancel() {
		voice.speak("FreeTTS is cancelled");
		voiceManager = null;
		voice = null;
		setCurrentState(state.INITIAL);
		return response.SUCCESS;
	}

	@Override
	protected void executeOutput(MMCmd event) {
		while(outputQueue.size()>5)outputQueue.pollFirst();
		voice.speak(event.getResponse().getMessage());
	}

}
