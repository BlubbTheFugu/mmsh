package integrated.audio;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

public class JSGFGrammarUpdater {
	
	public static void updateRoomNames(ArrayList<String> rooms){
		PrintWriter writer = null;
		try {
			writer = new PrintWriter("\\res\\grammar\\roomNames.gram", "UTF-8");
			writer.println("#JSGF V1.0;");
			writer.println("grammar roomNames;");
			writer.print("<room> = ");
			for(String name:rooms){
				writer.print(name+"|");
			}
			writer.print("\b;");
			writer.close();
			
			
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
