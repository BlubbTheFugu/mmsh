package integrated.gesture;

import java.util.ArrayList;
import java.util.Arrays;

import edu.ufl.digitalworlds.j4k.J4KSDK;
import edu.ufl.digitalworlds.j4k.Skeleton;
import event.MMCmd;
import event.MMCmdType;
import event.MMCmdType.cmdType;
import integrated.gesture.Gesture.gestureResponse;
import integrated.gestureImpl.DownGradeGesture;
import integrated.gestureImpl.OverheadWaveGesture;
import integrated.gestureImpl.PushDownGesture;
import integrated.gestureImpl.PushUpGesture;
import integrated.gestureImpl.UpGradeGesture;
import integrated.gestureImpl.WaveLeftGesture;
import integrated.gestureImpl.WaveRightGesture;
import javafx.scene.layout.Pane;
import main.MMInteractionManager;
import modality.Modality;
import modality.ModalityAttrib;
import models.SecurityModel;

public class GestureRecognizer extends Modality {
	
	private WaveRightGesture rightWave;
	private WaveLeftGesture leftWave;
	private PushDownGesture pushDown;
	private DownGradeGesture downgrade;
	private UpGradeGesture upgrade;
	private PushUpGesture pushUp;
	private OverheadWaveGesture overheadWave;
	
	private Kinectv2Recognizer kinect;
	private ArrayList<Gesture> gestureList = new ArrayList<Gesture>();
	
	public GestureRecognizer(int modID, ModalityAttrib attribute, MMInteractionManager mmi) {
		super(modID, attribute, mmi);
		gestureList.addAll( Arrays.asList(new WaveRightGesture(30),
									  new WaveLeftGesture(30),
									  new PushDownGesture(30),
									  new UpGradeGesture(40),
									  new DownGradeGesture(20),
									  new PushUpGesture(30),
									  new OverheadWaveGesture(20)));
	}

	@Override
	public response prepare() {
		if(kinect == null) {
			kinect = new Kinectv2Recognizer(this);
			kinect.start(J4KSDK.DEPTH | J4KSDK.COLOR | J4KSDK.SKELETON | J4KSDK.XYZ | J4KSDK.PLAYER_INDEX);
			return response.SUCCESS;
		}else return response.INVALID;
	}

	@Override
	public response cancel() {
		if(kinect == null)return response.INVALID;
		else {
			kinect.stop();
			kinect = null;
			return response.SUCCESS;
		}
	}
	
	public void recognizeGestures(Skeleton skeleton) {
		cmdType type = null;
		Pane pane = mmi.getViewContext();
		String view =  mmi.getViewContext().getClass().getSimpleName();
		String gestureName = "";
		ArrayList<Object> param = new ArrayList<>();
		for(Gesture gesture:gestureList) {
			if(gesture.update(skeleton) == gestureResponse.SUCCEEDED) {
				gestureName = gesture.getClass().getSimpleName();
				switch(gestureName) {
				case "WaveRightGesture":
					getLogger().info(view);
					if(view.equals("OverView")) {
						type = cmdType.SWITCH_ROOM_LIGHT;
					}else if(view.equals("SecurityView")){
						type = cmdType.SWITCH_SURVEILANCE;
						param = new ArrayList<Object>(Arrays.asList(!SecurityModel.getInstance().isSurveilance()));
					}else if(view.equals("EntertainmentView"))type = cmdType.NEXT;
					break;
				case "WaveLeftGesture":
					if(view.equals("OverView"))type = cmdType.CHANGE_TEMP;
					else if(view.equals("SecurityView")){
						type = cmdType.SWITCH_INTRUSION;
						param = new ArrayList<Object>(Arrays.asList(!SecurityModel.getInstance().isIntrusionAlarm()));
					}else if(view.equals("EntertainmentView"))type = cmdType.PREVIOUS;
					break;
				case "PushDownGesture":
					if(view.equals("OverView"))type = cmdType.CHANGE_TEMP;
					else if(view.equals("SecurityView")){
						type = cmdType.SWITCH_SMOKE_ALARM;
						param = new ArrayList<Object>(Arrays.asList(!SecurityModel.getInstance().isSmokeAlarm()));
					}
					else if(view.equals("EntertainmentView"))type = cmdType.PAUSE;
					break;
				case "PushUpGesture":
					if(view.equals("OverView"))type = cmdType.CHANGE_TEMP;
					else if(view.equals("SecurityView")){
						type = cmdType.SWITCH_GARAGE;
						param = new ArrayList<>(Arrays.asList(!SecurityModel.getInstance().isGarage()));
					}
					else if(view.equals("EntertainmentView"))type = cmdType.PLAY;
					break;
				case "OverHeadWaveGesture":
					if(view.equals("OverView"))type = cmdType.CHANGE_TEMP;
					else if(view.equals("EntertainmentView"))type = cmdType.STOP;
					break;
				}
				if(type!=null) {
					completeGesture(new MMCmd(modID,gestureName,pane,type,param));
					getLogger().info(" GestureInput: "+gestureName);
				}
			}
		}
	}
	
	private void completeGesture(MMCmd cmd) {
		mmi.addEvent(cmd);
		for(Gesture gesture:gestureList) {
			gesture.reset();
		}
	}

	@Override
	public response start() {
		kinect.start(J4KSDK.DEPTH | J4KSDK.COLOR | J4KSDK.SKELETON | J4KSDK.XYZ | J4KSDK.PLAYER_INDEX);
		return response.SUCCESS;
	}

	@Override
	public response pause() {
		kinect.stop();
		return response.SUCCESS;
	}

	@Override
	public response resume() {
		kinect.start(J4KSDK.DEPTH | J4KSDK.COLOR | J4KSDK.SKELETON | J4KSDK.XYZ | J4KSDK.PLAYER_INDEX);
		return response.SUCCESS;
	}
	
	


}
