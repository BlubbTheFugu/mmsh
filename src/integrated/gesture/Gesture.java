package integrated.gesture;

import java.util.ArrayList;
import edu.ufl.digitalworlds.j4k.Skeleton;

public abstract class Gesture {
	
	public enum gestureResponse {
		 SUCCEEDED, FAILED
	}

	protected ArrayList<GestureSegment> segments;
	protected int segmentCount;
	protected int frameCount;
	protected final int windowSize;

	public Gesture(int windowSize) {
		this.windowSize = windowSize;
		segments = new ArrayList<GestureSegment>();
		reset();
	}

	public void reset() {
		segmentCount = 0;
		frameCount = 0;
	}

	public gestureResponse update(Skeleton s) {
		gestureResponse partResult = segments.get(segmentCount).update(s);
        if (partResult == gestureResponse.SUCCEEDED) {
            if (segmentCount+1 < segments.size()) {
                segmentCount++;
                frameCount = 0;
                return gestureResponse.FAILED;
            } else {
            	reset();
            	return gestureResponse.SUCCEEDED;
            }
        } else if ( frameCount == windowSize) {
        	reset();
        	return gestureResponse.FAILED;
        } else {
            frameCount++;
            return gestureResponse.FAILED;
        }  
	}

}
