package integrated.gesture;

import edu.ufl.digitalworlds.j4k.Skeleton;
import integrated.gesture.Gesture.gestureResponse;

public interface GestureSegment {

	public gestureResponse update(Skeleton s);

}
