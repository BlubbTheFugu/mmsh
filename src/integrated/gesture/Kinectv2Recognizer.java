package integrated.gesture;

import java.util.Scanner;

import edu.ufl.digitalworlds.j4k.J4KSDK;
import edu.ufl.digitalworlds.j4k.Skeleton;
import util.Loggable;

public class Kinectv2Recognizer extends J4KSDK implements Loggable{
	
	private GestureRecognizer gestureRecognizer;
	
	public Kinectv2Recognizer(GestureRecognizer gestureRecognizer) {
		super();
		this.gestureRecognizer = gestureRecognizer;
	}
 
	@Override
	public void onColorFrameEvent(byte[] arg0) { }
 
	@Override
	public void onDepthFrameEvent(short[] arg0, byte[] arg1, float[] arg2, float[] arg3) { }
 
	@Override
	public void onSkeletonFrameEvent(boolean[] skeletons, float[] positions, float[] orientations, byte[] state) {
//		getLogger().info("Skeleton received");
		Skeleton skeleton = null;
		for (int i = 0; i < skeletons.length; i++) {
			if (skeletons[i]) {
				skeleton = Skeleton.getSkeleton(i, skeletons, positions, orientations, state, this);
			}
		}
		if (skeleton != null) {
			gestureRecognizer.recognizeGestures(skeleton);
		}
	}
	
}
