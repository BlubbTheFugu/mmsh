package integrated.visual;

import event.MMCmd;
import main.MMInteractionManager;
import modality.ModalityAttrib;
import modality.SimpleOutputModality;
import javafx.animation.Animation;
import javafx.animation.FadeTransition;
import javafx.animation.Interpolator;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.SequentialTransition;
import javafx.animation.Timeline;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.layout.StackPane;
import javafx.util.Duration;

public class OutputBox extends SimpleOutputModality{
	
	private Label output;
	private SequentialTransition transition;
	private StackPane textPane;
	private FadeTransition fader;
	
	public OutputBox(int modID, ModalityAttrib attribute, MMInteractionManager mmi){
		super(modID, attribute, mmi);
	}
	
	private Timeline createBlinker(Node node) {
	    Timeline blink = new Timeline(
	            new KeyFrame(
	                    Duration.seconds(10),
	                    new KeyValue(
	                            node.opacityProperty(), 
	                            1, 
	                            Interpolator.DISCRETE
	                    )
	            )
	    );
	    blink.setCycleCount(3);
	    return blink;
    }

    private FadeTransition createFader(Node node) {
        FadeTransition fade = new FadeTransition(Duration.seconds(5), node);
        fade.setFromValue(1);
        fade.setToValue(0);
        return fade;
    }

	@Override
	protected void executeOutput(MMCmd event) {
	    getLogger().info("Outputbox received"+ event.getResponse().getMessage());
		//if(transition!=null&&transition.getStatus()!=Animation.Status.RUNNING)transition.stop();
		if(event.getResponse().getMessage()!=null){
		    getLogger().info("Outputbox received"+ event.getResponse().getMessage());
		    output.setText(event.getResponse().getMessage());
		}
		transition.play();
		getLogger().info("asdafasdas");
	}

	@Override
	public response prepare() {
		textPane = new StackPane();
		textPane.setId("outputbox");
		output = new Label();
		textPane.getChildren().add(output);
		output.setStyle("-fx-text-fill: black; -fx-padding: 16px;");
		Timeline blinker = createBlinker(output);
		    //blinker.setOnFinished(event -> output.setText("Fading"));
		fader = createFader(output);
	    transition = new SequentialTransition(
	    		output,
	            blinker,
	            fader
	    );
		return response.SUCCESS;
	}

	@Override
	public response cancel() {
		if(transition!=null&&transition.getStatus()!=Animation.Status.RUNNING)transition.stop();
		if(output!=null)output.setText("");
		transition = null;
		fader = null;
		textPane = null;
		return response.SUCCESS;
	}
	
	public Node getOutputPane(){
		return textPane;
	}
 
}
