package integrated.gestureSegement;
import edu.ufl.digitalworlds.j4k.Skeleton;
import integrated.gesture.Gesture.gestureResponse;
import integrated.gesture.GestureSegment;

public class RightThumbLeftOfTip implements GestureSegment{

	@Override
	public gestureResponse update(Skeleton s) {
		if(s.get3DJointY(Skeleton.HAND_RIGHT) > s.get3DJointY(Skeleton.ELBOW_RIGHT)) {			
//			if(s.get3DJointY(Skeleton.THUMB_RIGHT) == s.get3DJointY(Skeleton.HAND_TIP_RIGHT)) {
				if(s.get3DJointX(Skeleton.THUMB_RIGHT) < s.get3DJointX(Skeleton.HAND_TIP_RIGHT)) {
					return gestureResponse.SUCCEEDED;
				}
//			}
		}
		return gestureResponse.FAILED;
	}

}
