package integrated.gestureSegement;

import edu.ufl.digitalworlds.j4k.Skeleton;
import integrated.gesture.Gesture.gestureResponse;
import integrated.gesture.GestureSegment;

public class LeftHandRightAboveEllbow implements GestureSegment {

	@Override
	public gestureResponse update(Skeleton s) {
		if(s.get3DJointY(Skeleton.HAND_LEFT) > s.get3DJointY(Skeleton.ELBOW_LEFT)) {
			if(s.get3DJointY(Skeleton.HAND_LEFT) < s.get3DJointY(Skeleton.HEAD)) {
				if(s.get3DJointX(Skeleton.HAND_LEFT) > s.get3DJointX(Skeleton.ELBOW_LEFT))
					return gestureResponse.SUCCEEDED;
			}
		}
		return gestureResponse.FAILED;
	}

}
