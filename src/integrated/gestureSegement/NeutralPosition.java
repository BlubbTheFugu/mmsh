package integrated.gestureSegement;

import edu.ufl.digitalworlds.j4k.Skeleton;
import integrated.gesture.GestureSegment;
import integrated.gesture.Gesture.gestureResponse;

public class NeutralPosition implements GestureSegment{

	@Override
	public gestureResponse update(Skeleton s) {
		if(s.get3DJointY(Skeleton.HAND_RIGHT) < s.get3DJointY(Skeleton.ELBOW_RIGHT)) {			
			if(s.get3DJointY(Skeleton.HAND_LEFT) < s.get3DJointY(Skeleton.ELBOW_LEFT)) {
				return gestureResponse.SUCCEEDED;
			}
		}
		return gestureResponse.FAILED;
	}

}
