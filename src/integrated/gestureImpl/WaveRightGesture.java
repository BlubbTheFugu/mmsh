package integrated.gestureImpl;

import java.util.Arrays;

import integrated.gesture.Gesture;
import integrated.gesture.GestureSegment;
import integrated.gestureSegement.NeutralPosition;
import integrated.gestureSegement.RightHandLeftAboveEllbow;
import integrated.gestureSegement.RightHandRightAboveEllbow;

public class WaveRightGesture extends Gesture{
	
	public WaveRightGesture(int windowSize) {
		super(windowSize);
		NeutralPosition neutral = new NeutralPosition();
		GestureSegment seg1 = new RightHandLeftAboveEllbow(); 
		GestureSegment seg2 = new RightHandRightAboveEllbow();
		segments.addAll(Arrays.asList(neutral,seg1,seg2,seg1,seg2,neutral));
	}
	
}
