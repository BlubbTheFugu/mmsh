package integrated.gestureImpl;

import java.util.Arrays;

import integrated.gesture.Gesture;
import integrated.gesture.GestureSegment;
import integrated.gestureSegement.LeftHandLeftAboveEllbow;
import integrated.gestureSegement.LeftHandRightAboveEllbow;
import integrated.gestureSegement.NeutralPosition;

public class WaveLeftGesture extends Gesture{
	
	public WaveLeftGesture(int windowSize) {
		super(windowSize);
		NeutralPosition neutral = new NeutralPosition();
		GestureSegment seg1 = new LeftHandRightAboveEllbow();
		GestureSegment seg2 = new LeftHandLeftAboveEllbow(); 
		segments.addAll(Arrays.asList(neutral,seg1,seg2,seg1,seg2,neutral));
	}
	
}
