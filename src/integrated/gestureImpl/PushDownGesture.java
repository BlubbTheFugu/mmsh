package integrated.gestureImpl;

import java.util.Arrays;

import integrated.gesture.Gesture;
import integrated.gesture.GestureSegment;
import integrated.gestureSegement.HandsAboveHead;
import integrated.gestureSegement.HandsBelowHead;
import integrated.gestureSegement.NeutralPosition;

public class PushDownGesture extends Gesture{
	
	public PushDownGesture(int windowSize) {
		super(windowSize);
		NeutralPosition neutral = new NeutralPosition();
		GestureSegment seg1 = new HandsAboveHead(); 
		GestureSegment seg2 = new HandsBelowHead();
		segments.addAll(Arrays.asList(neutral,seg1,seg2,seg1,seg2,neutral));
	}
	
}
