package integrated.gestureImpl;

import java.util.Arrays;

import integrated.gesture.Gesture;
import integrated.gesture.GestureSegment;
import integrated.gestureSegement.HandsAboveHeadBetweenEllbows;
import integrated.gestureSegement.HandsAboveHeadNotBetweenEllbows;
import integrated.gestureSegement.NeutralPosition;


public class OverheadWaveGesture extends Gesture{
	
	public OverheadWaveGesture(int windowSize) {
		super(windowSize);
		NeutralPosition neutral = new NeutralPosition();
		GestureSegment seg1 = new HandsAboveHeadBetweenEllbows();
		GestureSegment seg2 = new HandsAboveHeadNotBetweenEllbows();
		segments.addAll(Arrays.asList(neutral,seg1,seg2,seg1,seg2,neutral));
	}
	
}
