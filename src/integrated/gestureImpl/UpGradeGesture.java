package integrated.gestureImpl;

import java.util.Arrays;
import integrated.gesture.Gesture;
import integrated.gesture.GestureSegment;
import integrated.gestureSegement.RightThumbHigherTip;
import integrated.gestureSegement.RightThumbLeftOfTip;
import integrated.gestureSegement.RightThumbLowerTip;

public class UpGradeGesture extends Gesture{
	
	public UpGradeGesture(int windowSize) {
		super(windowSize);
		GestureSegment seg1 = new RightThumbHigherTip(); 
		GestureSegment seg2 = new RightThumbLeftOfTip();
		GestureSegment seg3 = new RightThumbLowerTip(); 
		segments.addAll(Arrays.asList(seg3,seg2,seg1,seg2,seg3,seg2,seg1,seg2,seg3));
	}
	
}