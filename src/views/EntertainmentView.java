package views;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import util.DialogHandler;
import util.Loggable;
import util.Song;
import de.jensd.fx.glyphs.GlyphIcon;
import de.jensd.fx.glyphs.GlyphsDude;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIcon;
import event.MMCmdInterface;
import event.MMCmdType.cmdType;
import event.MMResponse;
import main.Control;
import main.Control.model;
import modality.Modality.response;
import models.EntertainmentModel;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.geometry.Side;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.SingleSelectionModel;
import javafx.scene.control.Slider;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.ToolBar;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.BorderPane;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.media.MediaPlayer.Status;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import javafx.util.Duration;

public class EntertainmentView extends BorderPane implements Loggable,DialogHandler,MMCmdInterface{
	
	private Control control;
	private ToolBar toolbar;
	private Slider sVolume;
	private Button bAddSongs,bAddPlaylist,bDeletePlaylist, bPause, bMute, bRandom;
	private TabPane playlistTabPane;
	private ProgressBar progress = new ProgressBar();
	
	private ArrayList<TableView<Song>> playlistTables = new ArrayList<TableView<Song>>();
	private TableView<Song> activeTable;
	private int songIndex;
	MediaPlayer player;

	private boolean randomValue=false;
	final Object obj= new Object();
	private ChangeListener<Number> volumeListener = new ChangeListener<Number>(){
		@Override
		public void changed(ObservableValue<? extends Number> observable,
				Number oldValue, Number newValue) {
			control.addInputEvent(cmdType.SET_VOLUME, new ArrayList<>(Arrays.asList(newValue.doubleValue())));
		}			
	};
	
	public EntertainmentView(Control c, Color color){
		this.control = c;
		this.setTop(createToolBar());
		this.setLeft(createPlaylistTabPane());
		this.setBottom(createPlayerBar());
	}
	
	private TabPane	createPlaylistTabPane(){
		playlistTabPane = new TabPane();
        playlistTabPane.setPrefSize(600, 400);
        playlistTabPane.setSide(Side.TOP);
        ArrayList<String> playlistNames = ((EntertainmentModel)control.getModel(model.ENTERTAINMENT)).getPlaylistNames();
        playlistTabPane.setTabClosingPolicy(TabPane.TabClosingPolicy.UNAVAILABLE);
    	for (int i = 0; i < playlistNames.size(); i++) {
    		Tab newTab = getPlaylistTab(i);
    		newTab.setText(playlistNames.get(i));
    		playlistTabPane.getTabs().add(newTab);
		}
    	activeTable = playlistTables.get(0);
    	songIndex = 0;
    	if(activeTable!=null&&!activeTable.getItems().isEmpty())player = new MediaPlayer(new Media(activeTable.getItems().get(songIndex).getPath()));
    	return playlistTabPane;
	}
	
	private Tab getPlaylistTab(int playlist_id){
		final Tab newTab = new Tab();
		TableView<Song> table = createSongListTable(playlist_id);
        newTab.setContent(table);
        newTab.setOnSelectionChanged(new EventHandler<Event>() {
            @Override
            public void handle(Event event) {
              	Tab t = (Tab)event.getSource();
              	if(t.isSelected()){
              		int index = playlistTabPane.getSelectionModel().getSelectedIndex();
//              		getLogger().info(t.getText()+ " "+index);
              		//activePlaylist = playlistTables.get(index);
              		//activeTable = playlistTables.get(index);
              	}	              
            }
        });
        return newTab;
	}
	
	private TableView<Song> createSongListTable(int playlist_id){
		TableView<Song> table = createSongTable();
		ArrayList<Song> songs = ((EntertainmentModel)control.getModel(model.ENTERTAINMENT)).getSongForPlaylistID(playlist_id);
		if(songs!=null&&!songs.isEmpty())table.setItems(FXCollections.observableArrayList(songs));
		else table.setItems(FXCollections.observableArrayList());
		playlistTables.add(table);
		return table;
	}
	
	private TableView<Song> createSongTable(){
		TableColumn<Song, String> songName = new TableColumn<Song, String>();
		songName.setText("Title");
		songName.setCellValueFactory(new PropertyValueFactory<Song, String>("title"));
		TableColumn<Song, String> album = new TableColumn<Song, String>();
		album.setText("Album");
		album.setCellValueFactory(new PropertyValueFactory<Song, String>("album"));
		TableColumn<Song, String> interpret = new TableColumn<Song, String>();
		interpret.setText("Artist");
		interpret.setMinWidth(100);
		interpret.setCellValueFactory(new PropertyValueFactory<Song, String>("artist"));
		TableColumn<Song, String> duration = new TableColumn<Song, String>();
		duration.setText("Duration");
		duration.setPrefWidth(50);
		duration.setCellValueFactory(new PropertyValueFactory<Song, String>("duration"));
		TableView<Song> tableView = new TableView<Song>();
		tableView.getColumns().addAll(songName, album, interpret,duration);
		tableView.getSelectionModel().selectedItemProperty().addListener((observableValue, oldValue, newValue) -> {
	        if (tableView.getSelectionModel().getSelectedItem() != null) {
	        	bPause.setGraphic(GlyphsDude.createIcon(FontAwesomeIcon.PAUSE));
	        	int index = playlistTabPane.getSelectionModel().getSelectedIndex();
          		activeTable = playlistTables.get(index);
          		songIndex = activeTable.getSelectionModel().getSelectedIndex();
	        	EntertainmentView.this.reinitPlayer(activeTable.getItems().get(songIndex).getPath());
	        	player.play();
	        }
        });
		
		return tableView;
	}
	
	private ToolBar createToolBar() {
	    toolbar = new ToolBar();
	    bAddSongs = GlyphsDude.createIconButton(FontAwesomeIcon.FILE_AUDIO_ALT,"Add Songs","15px;","13px",ContentDisplay.LEFT);
	    bAddSongs.setOnAction((event) -> {
			List<File> songs = addSongChooser();
			if(songs!=null){
				for (File s:songs) {
					Media file = new Media(s.toURI().toString());  
		            // display media's metadata
		            for (Map.Entry<String, Object> entry : file.getMetadata().entrySet()){
		                getLogger().info(entry.getKey() + ": " + entry.getValue());
		            }
					addSongToList(s.toURI().toString());
		            synchronized(obj){
		               try {
		            	   obj.wait(100);
		               } catch (Exception e) {
		            	   e.printStackTrace();
		               }
		            }
				}				
			}
		});
		bAddPlaylist = GlyphsDude.createIconButton(FontAwesomeIcon.PLUS,"Add playlist","15px;","13px",ContentDisplay.LEFT);
		bAddPlaylist.setOnAction((event) -> {
			if(playlistTables.size()>1)bDeletePlaylist.setDisable(false);
			control.addInputEvent(cmdType.ADD_PLAYLIST, new ArrayList<>());
		});
		bDeletePlaylist = GlyphsDude.createIconButton(FontAwesomeIcon.MINUS,"Delete playlist","15px;","13px",ContentDisplay.LEFT);
		if(playlistTables.size()<2)bDeletePlaylist.setDisable(true);
		bDeletePlaylist.setOnAction((event) -> {
			if(playlistTables.size()<=2)bDeletePlaylist.setDisable(true);
			control.addInputEvent(cmdType.DELETE_PLAYLIST, new ArrayList<Object>(Arrays.asList(playlistTabPane.getSelectionModel().getSelectedItem().getText())));				
			
		});
		toolbar.getItems().addAll(bAddSongs,bAddPlaylist,bDeletePlaylist);
		return toolbar;
	}
	
	private ToolBar createPlayerBar(){
		ToolBar playerBar = new ToolBar();
		bRandom = GlyphsDude.createIconButton(FontAwesomeIcon.RANDOM);
		bRandom.setOnAction(event -> {
			if(randomValue)control.addInputEvent(cmdType.RANDOM_OFF, new ArrayList<Object>(Arrays.asList()));
			else control.addInputEvent(cmdType.RANDOM_ON, new ArrayList<Object>(Arrays.asList()));
		});
		Button stop = GlyphsDude.createIconButton(FontAwesomeIcon.STOP);
		stop.setOnAction(event -> {
			control.addInputEvent(cmdType.STOP, new ArrayList<Object>(Arrays.asList()));
		});
		Button backwards = GlyphsDude.createIconButton(FontAwesomeIcon.FAST_BACKWARD);
		backwards.setOnAction(event -> {
			control.addInputEvent(cmdType.BACKWARDS, new ArrayList<Object>(Arrays.asList(10)));
		});
		Button previous = GlyphsDude.createIconButton(FontAwesomeIcon.BACKWARD);
		previous.setOnAction(event -> {
			control.addInputEvent(cmdType.PREVIOUS, new ArrayList<Object>(Arrays.asList()));
		});
		bPause = GlyphsDude.createIconButton(FontAwesomeIcon.PLAY);
		bPause.setOnAction(event -> {
			if(player.getStatus()!=Status.PLAYING){
				control.addInputEvent(cmdType.PLAY, new ArrayList<Object>(Arrays.asList()));
			}else {
				control.addInputEvent(cmdType.PAUSE, new ArrayList<Object>(Arrays.asList()));
			}
		});
		Button next = GlyphsDude.createIconButton(FontAwesomeIcon.FORWARD);
		next.setOnAction(event ->  {
			control.addInputEvent(cmdType.NEXT, new ArrayList<Object>(Arrays.asList()));
		});
		Button forwards = GlyphsDude.createIconButton(FontAwesomeIcon.FAST_FORWARD);
		forwards.setOnAction(event -> {	
			control.addInputEvent(cmdType.FORWARDS, new ArrayList<Object>(Arrays.asList(10)));
		});
		bMute = GlyphsDude.createIconButton(FontAwesomeIcon.VOLUME_UP);
		bMute.setOnAction(event -> {
			if(player.isMute())control.addInputEvent(cmdType.MUTE_OFF, new ArrayList<Object>(Arrays.asList()));
			else control.addInputEvent(cmdType.MUTE_ON, new ArrayList<Object>(Arrays.asList()));
		});		
		sVolume = new Slider();
		sVolume.setMin(0.0);
		sVolume.setMax(1.0);
		sVolume.setValue(1.0);
		sVolume.setShowTickLabels(false);
		sVolume.setShowTickMarks(true);
		sVolume.setMajorTickUnit(0.1);
		sVolume.setMinorTickCount(10);
		sVolume.setBlockIncrement(0.01);
		sVolume.valueProperty().addListener(volumeListener);
				
		progress = new ProgressBar();
		progress.setProgress(0);
		playerBar.getItems().addAll(bRandom,stop,backwards,previous,bPause,next,forwards,bMute,sVolume,progress);
		return playerBar;
	}
	
	@Override
	public MMResponse addPlaylist(String playlistName) {
		final Tab newTab = getPlaylistTab(playlistTables.size());
		newTab.setText(playlistName);
		playlistTabPane.getTabs().add(newTab);
		return new MMResponse("Added playlist "+playlistName);
	}
	
	@Override
	public MMResponse deletePlaylist(String playlistName) {
		if(playlistTabPane.getTabs().size()>1){
			playlistTabPane.getTabs().remove(playlistTabPane.getSelectionModel().getSelectedIndex());
			playlistTables.remove(playlistTabPane.getSelectionModel().getSelectedIndex());
			return new MMResponse("Deleted playlist "+playlistName);
		}else return MMCmdInterface.super.deletePlaylist(playlistName);
		
	}
	
	private List<File> addSongChooser(){
		FileChooser fileChooser = new FileChooser();
		fileChooser.setTitle("Select MP3 files");
		fileChooser.getExtensionFilters().addAll(new ExtensionFilter("MP3", "*.mp3"));
		List<File> selectedFiles = fileChooser.showOpenMultipleDialog(null);
		if (selectedFiles != null) {
			return selectedFiles;
		}else {
			return null;
		}
	}
	
	private void addSongToList(String mediaURI){
		Media m = new Media(mediaURI);
	    final MediaPlayer mp = new MediaPlayer(m); 
//	    getLogger().info(m.getSource());
	    mp.setOnReady(new Runnable() {
	        @Override
	        public void run() {
	            String title = (String) mp.getMedia().getMetadata().get("title");
	            if(title==null||title.isEmpty())title = new File(mediaURI).getName().replace("%20", " ");
	            String artist = (String) mp.getMedia().getMetadata().get("artist");
	            String album = (String) mp.getMedia().getMetadata().get("album");
	            Duration duration =  mp.getMedia().getDuration();
	            Song newSong = new Song(title,artist,album,(int)duration.toMillis(),m.getSource());
	            int selectedPlaylist = playlistTabPane.getSelectionModel().getSelectedIndex();
	            playlistTables.get(selectedPlaylist).getItems().add(newSong);
	            EntertainmentModel.getInstance().addSongToPlaylist(selectedPlaylist, newSong);
	            synchronized(obj){//this is required since mp.setOnReady creates a new thread and our loopp  in the main thread
	                obj.notify();// the loop has to wait unitl we are able to get the media metadata thats why use .wait() and .notify() to synce the two threads(main thread and MediaPlayer thread)
	            }
	        }
	    });
	}

	@Override
	public MMResponse play() {
		if(player!=null){
			bPause.setGraphic(GlyphsDude.createIcon(FontAwesomeIcon.PAUSE));
			getLogger().info(player.getStatus());
			if(player.getStatus()==Status.READY||player.getStatus()==Status.PAUSED||player.getStatus()==Status.STOPPED){
				player.play();
				return new MMResponse("Playback continued");
			}else return new MMResponse("Already playing");
		}else return MMCmdInterface.super.play();
	}

	private void reinitPlayer(String path){
		Media m = new Media(path);
		if(player==null)player = new MediaPlayer(m);
		if(player.getStatus()==Status.PLAYING)player.stop();
	  	double volume = sVolume.getValue();
    	boolean mute = player.isMute();
		player = new MediaPlayer(m);
    	player.setVolume(volume);
    	player.setMute(mute);
    	player.setOnEndOfMedia(new Runnable(){
			@Override
			public void run() {
				control.addInputEvent(cmdType.NEXT, new ArrayList<>());
			}
    	});
	    player.currentTimeProperty().addListener(new ChangeListener<Duration>() {
	    	@Override public void changed(ObservableValue<? extends Duration> observableValue, Duration oldValue, Duration newValue) {
	    		double value = player.getCurrentTime().toMillis() / player.getTotalDuration().toMillis();
	    		progress.setProgress(value);
	    	}
	    });
	}

	@Override
	public MMResponse pause() {
		if(player!=null){
			bPause.setGraphic(GlyphsDude.createIcon(FontAwesomeIcon.PLAY));
			if(player.getStatus()==Status.PLAYING)player.pause();
			return new MMResponse("Playback was paused");
		}else return MMCmdInterface.super.pause();
	}

	@Override
	public MMResponse stop() {
		if(player!=null){
			bPause.setGraphic(GlyphsDude.createIcon(FontAwesomeIcon.PLAY));
			if(player.getStatus()==Status.PLAYING)player.stop();
			return new MMResponse("Playback was stopped");
		}else return MMCmdInterface.super.stop();
	}

	@Override
	public MMResponse next() {
		if(player!=null){
			bPause.setGraphic(GlyphsDude.createIcon(FontAwesomeIcon.PAUSE));
			if(randomValue)songIndex = (int) (Math.random()*activeTable.getItems().size());
			else songIndex++;
			if(songIndex>=activeTable.getItems().size())songIndex = 0;
			activeTable.getSelectionModel().select(songIndex);
			return new MMResponse("Next track");
		}else return MMCmdInterface.super.next();
	}

	@Override
	public MMResponse previous() {
		if(player!=null){
			bPause.setGraphic(GlyphsDude.createIcon(FontAwesomeIcon.PAUSE));
			if(randomValue)songIndex = (int) (Math.random()*activeTable.getItems().size());
			else songIndex--;
			if(songIndex==-1)songIndex = activeTable.getItems().size()-1;
			activeTable.getSelectionModel().select(songIndex);
			return new MMResponse("Previous track");
		}else return MMCmdInterface.super.previous();
	}

	@Override
	public MMResponse setMute(boolean mute) {
		if(player!=null){
			player.setMute(mute);
			if(mute){
				bMute.setGraphic(GlyphsDude.createIcon(FontAwesomeIcon.VOLUME_OFF));	
				return new MMResponse("Playback was muted");
			}else{
				bMute.setGraphic(GlyphsDude.createIcon(FontAwesomeIcon.VOLUME_UP));
				return new MMResponse("Playback was unmuted");
			}
		}else return MMCmdInterface.super.setMute(mute);
	}

	@Override
	public MMResponse setVolume(double volume) {
		if(player!=null){
			sVolume.valueProperty().removeListener(volumeListener);
			sVolume.setValue(volume);
			player.setVolume(volume);
			sVolume.valueProperty().addListener(volumeListener);
			return new MMResponse("Volume was set to "+(int)(volume*100)+" percent");
		}else return MMCmdInterface.super.setVolume(volume);
	}
	
	@Override
	public MMResponse raiseVolume(double volume) {
		if(player!=null){
			setVolume(sVolume.getValue()+volume);
			return new MMResponse("Volume was raise "+volume+" percent");
		}else return MMCmdInterface.super.raiseVolume(volume);
	}

	@Override
	public MMResponse lowerVolume(double volume) {
		if(player!=null){
			setVolume(sVolume.getValue()-volume);
			return new MMResponse("Volume was lowered "+volume+" percent");
		}else return MMCmdInterface.super.lowerVolume(volume);
	}

	@Override
	public MMResponse setRandom(boolean random) {
		randomValue = random;
		if(random)return new MMResponse("Random playbackmode was enabled");
		else return new MMResponse("Random playbackmode was disabled");
	}

	@Override
	public MMResponse forwardPlayback(int seconds) {
		if(player!=null){
			Duration d = player.getCurrentTime().add(Duration.seconds(seconds));
			if(!d.isUnknown()&&!d.isIndefinite())player.seek(d);
			else next();
			return new MMResponse("Playback was forwarded "+seconds+" seconds");
		}else return MMCmdInterface.super.forwardPlayback(seconds);
	}

	@Override
	public MMResponse backwardPlayback(int seconds) {
		if(player!=null){
			Duration d = player.getCurrentTime().subtract((Duration.seconds(seconds)));
			if(!d.isUnknown()&&!d.isIndefinite())player.seek(d);
			else previous();
			return new MMResponse("Playback was backwarded "+seconds+" seconds");
		}else return MMCmdInterface.super.backwardPlayback(seconds);
	}


	
}

