package views;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import event.MMCmdInterface;
import javafx.scene.control.ToolBar;
import javafx.scene.layout.BorderPane;
import javafx.scene.paint.Color;
import main.Control;
import util.Loggable;

public class ModalityView extends BorderPane implements Loggable,MMCmdInterface{
	
	private Control control;
	private ToolBar toolbar;

	private Map<String,Room> roomPane;
	
	public ModalityView(Control c,Color color){
		this.control = c;
		setMinSize(600, 400);
		getStylesheets().add("view.css");
		setId("view");

		this.setTop(createToolBar());		
//		this.setCenter();
 		this.setRight(rightToolbar());
	}
	
    private ToolBar createToolBar() {
	    ToolBar toolbar = new ToolBar();
		
// 		toolbar.getItems().addAll();
 		return toolbar;
    }
      
    private ToolBar rightToolbar(){
    	ToolBar t = new ToolBar();
    	
// 		t.getItems().addAll(sTemperature,lightSourceLabel,cLightSource);
// 		t.setOrientation(Orientation.VERTICAL);
// 		this.getChildren().add(toolbar);
 		return t;
    }
   
	
}