package views;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.WordUtils;

import util.Loggable;
import de.jensd.fx.glyphs.GlyphsDude;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIcon;
import event.MMCmdInterface;
import event.MMCmd;
import event.MMCmdType.cmdType;
import event.MMResponse;
import main.Control;
import main.Control.model;
import main.GUI;
import modality.Modality.response;
import models.RoomModel;
import models.BlindsModel;
import models.LightModel;
import models.TemperatureModel;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.Separator;
import javafx.scene.control.Slider;
import javafx.scene.control.Toggle;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.ToolBar;
import javafx.scene.effect.GaussianBlur;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Text;


public class OverView extends BorderPane implements Loggable, MMCmdInterface{
	
	private Control control;
	private ToolBar toolbar;
	private ComboBox<String> cRoomNames, cLightSource;
	private Slider sTemperature;
	private Button addRoom,deleteRoom,newLayout,switchLighting;
	private RadioButton blindsDown, blindsUp, lightOn, lightOff;
	private ToggleGroup blindsGroup, lightGroup;
	private ObservableList<String> rooms,light_sources;
	private Pane roomPane;
	private TemperatureChangeListener tempListener = new TemperatureChangeListener();
	private BlindsChangeListener blindsListener = new BlindsChangeListener();
	private LightChangeListener lightListener = new LightChangeListener();
	private String selectedRoom;
	
	public OverView(Control c,Color color){
		this.control = c;
		setMinSize(600, 400);
		getStylesheets().add("view.css");
		setId("view");
		rooms = FXCollections.observableArrayList(((RoomModel)control.getModel(model.ROOM)).getRoomNames());
		selectedRoom = rooms.get(0);
		light_sources = FXCollections.observableArrayList(((LightModel)control.getModel(model.LIGHT)).getDescriptionForRoomName(selectedRoom));
		this.setTop(createToolBar());		
		this.setCenter(createRoomPane());
 		this.setRight(rightToolbar());
	}
	
    private ToolBar createToolBar() {
	    ToolBar toolbar = new ToolBar();
		cRoomNames = new ComboBox<String>();	
 		cRoomNames.setItems(rooms);
 		cRoomNames.setValue("Choose room");
 		cRoomNames.setEditable(true);
 		cRoomNames.valueProperty().addListener(new ChangeListener<String>() {
 			@Override public void changed(ObservableValue ov, String before, String after) {
 				if(!before.equals(after)&&!after.equals(selectedRoom)){
 					getLogger().info("Before: "+before+ "After: "+after+" Selected Room:"+ selectedRoom);			
 					if(rooms.contains(after)){
 						selectedRoom = after;
 						control.addInputEvent(cmdType.SWITCH_ROOM,new ArrayList<>(Arrays.asList(selectedRoom)));
 					}
 				}
 			}    
 	    });
 		addRoom = GlyphsDude.createIconButton(FontAwesomeIcon.PLUS_SQUARE,"Add room","15px;","13px",ContentDisplay.LEFT);
		addRoom.setOnAction((event) -> {
			String value = cRoomNames.getSelectionModel().getSelectedItem();
			if(!rooms.contains(value)&&cRoomNames.getValue()!="Choose room"){
				control.addInputEvent(cmdType.ADD_ROOM, new ArrayList<>(Arrays.asList(cRoomNames.getValue())));				
			}
		});
		deleteRoom = GlyphsDude.createIconButton(FontAwesomeIcon.MINUS_SQUARE,"Delete room","15px;","13px",ContentDisplay.LEFT);
		deleteRoom.setOnAction((event) -> {
			String value = cRoomNames.getSelectionModel().getSelectedItem();
			if(rooms.contains(value)){
				control.addInputEvent(cmdType.DELETE_ROOM, new ArrayList<>(Arrays.asList(cRoomNames.getValue())));
			}
		});
		newLayout = GlyphsDude.createIconButton(FontAwesomeIcon.MAP,"New layout","15px;","13px",ContentDisplay.LEFT);
		newLayout.setOnAction((event) -> {
			control.addInputEvent(cmdType.NEW_LAYOUT, new ArrayList<>(Arrays.asList()));
		});
		
 		toolbar.getItems().addAll(cRoomNames,addRoom,deleteRoom);
 		return toolbar;
    }
   
    private Pane createRoomPane(){
    	roomPane = new Pane();
 		roomPane.setPrefSize(300, 300);
 		roomPane.setId("roomPane");
 		
 		int cnt=0;
 		for(Room r:((RoomModel)control.getModel(model.ROOM)).getRooms()){
 			r.setOnMouseClicked(new EventHandler<MouseEvent>() {
				@Override
				public void handle(MouseEvent event) {
					if(selectedRoom!=r.getName()){
						selectedRoom = r.getName();
						r.setSelected(true);
						control.addInputEvent(cmdType.SWITCH_ROOM, new ArrayList<>(Arrays.asList(r.getName())));
					}
				}
 	        });
 			roomPane.getChildren().add(r);
 			cnt++;
 		}
 		return roomPane;
    }
    
    private ToolBar rightToolbar(){
    	ToolBar t = new ToolBar();
    	t.setPadding(new Insets(10,10,10,10));
    	Label temperatureLabel = GlyphsDude.createIconLabel(FontAwesomeIcon.FIRE,"Temperature","15px;","13px",ContentDisplay.LEFT);	
 		sTemperature = new Slider();
 		sTemperature.setMin(0);
 		sTemperature.setMax(30);
 		sTemperature.setShowTickLabels(true);
 		sTemperature.setShowTickMarks(true);
 		sTemperature.setMajorTickUnit(10);
 		sTemperature.setMinorTickCount(1);
 		sTemperature.setBlockIncrement(1);
 		sTemperature.setDisable(true);
// 		final GaussianBlur gaussianBlur = new GaussianBlur();
// 		gaussianBlur.setRadius(3d);
//      sTemperature.setEffect(gaussianBlur);
 		sTemperature.valueProperty().addListener(tempListener);
 		Label lightSourceLabel= GlyphsDude.createIconLabel(FontAwesomeIcon.LIGHTBULB_ALT,"Lighting","15px;","13px",ContentDisplay.LEFT);	
		cLightSource = new ComboBox<String>();
		cLightSource.setValue("Choose light source");
 		
 		Button bAddLightSource = GlyphsDude.createIconButton(FontAwesomeIcon.PLUS,"Add lightsource","15px;","13px",ContentDisplay.LEFT);
		bAddLightSource.setOnAction((event) -> {
//			control.addInputEvent(cmdType.ADD_PLAYLIST, new ArrayList<>());
		});
		lightGroup = new ToggleGroup();
		VBox lightBox = new VBox();
	    lightBox.setSpacing(5);
        lightOn = new RadioButton("On");
        lightOn.setDisable(true);
        lightOn.setToggleGroup(lightGroup);    
        lightOff = new RadioButton("Off");
        lightOff.setToggleGroup(lightGroup);
     	lightOff.setSelected(true);
     	lightOff.setDisable(true);
        lightGroup.selectedToggleProperty().addListener(lightListener);
        lightBox.getChildren().addAll(lightOn,lightOff);
		
		Label blindsLabel= GlyphsDude.createIconLabel(FontAwesomeIcon.NAVICON,"Blinds","15px;","13px",ContentDisplay.LEFT);
		blindsGroup = new ToggleGroup();
		VBox blindsBox = new VBox();
	    blindsBox.setSpacing(5);
        blindsUp = new RadioButton("Up");
        blindsUp.setDisable(true);
        blindsUp.setToggleGroup(blindsGroup);    
        blindsDown = new RadioButton("Down");
        blindsDown.setToggleGroup(blindsGroup);
        blindsDown.setSelected(true);
        blindsDown.setDisable(true);
        blindsGroup.selectedToggleProperty().addListener(blindsListener);
        blindsBox.getChildren().addAll(blindsUp,blindsDown);
		t.getItems().addAll(temperatureLabel,sTemperature,new Separator(),lightSourceLabel,lightBox,new Separator(),blindsLabel,blindsBox);
 		t.setOrientation(Orientation.VERTICAL);
 		return t;
    }
    
    @Override
	public MMResponse selectRoom(String roomName) {
    	cRoomNames.setValue(roomName);
    	for(int i=0;i<roomPane.getChildren().size();i++){
    		Room r = (Room) roomPane.getChildren().get(i);
    		if(r.getName()==roomName)r.setSelected(true);
    		else r.setSelected(false);
    	}
    	selectedRoom = roomName;
    	light_sources = FXCollections.observableArrayList(LightModel.getInstance().getDescriptionForRoomName(selectedRoom));
    	if(light_sources!=null)cLightSource.setItems(light_sources);
    	selectLightSource(light_sources.get(0));
    	sTemperature.setDisable(false);	
    	changeTemperature(selectedRoom,TemperatureModel.getInstance().getTempForRoom(selectedRoom));
    	blindsDown.setDisable(false);
    	blindsUp.setDisable(false);
    	lightOn.setDisable(false);
    	lightOff.setDisable(false);
    	changeLightState(selectedRoom,LightModel.getInstance().getOverheadLightForRoomName(selectedRoom));
    	changeBlindState(selectedRoom,BlindsModel.getInstance().getBlindsForRoom(selectedRoom));
    	return new MMResponse("Room "+roomName+" was selected");    
	}

	public void selectLightSource(String lightSource){
    	cLightSource.setValue(lightSource);
    }
     
    @Override
    public MMResponse changeLightState(String roomName,boolean state) {
        if(roomName.equals(selectedRoom)){
            lightGroup.selectedToggleProperty().removeListener(lightListener);
            if(state)lightOn.setSelected(true);
            else lightOff.setSelected(true);
            lightGroup.selectedToggleProperty().addListener(lightListener);
        }
        if(state)return new MMResponse("Turned light in room "+roomName+" on");
        else return new MMResponse("Turned light in room "+roomName+" off");
    }

    @Override
	public MMResponse changeTemperature(String roomName, int tmp) {
    	sTemperature.valueProperty().removeListener(tempListener);
    	sTemperature.setValue(tmp);
    	sTemperature.valueProperty().addListener(tempListener);
    	return new MMResponse("Changed temperature for "+roomName+" to "+tmp+" degrees");
	}
    
    @Override
	public MMResponse changeBlindState(String roomName, boolean state) {
        if(roomName.equals(selectedRoom)){
            blindsGroup.selectedToggleProperty().removeListener(blindsListener);
            if(state)blindsUp.setSelected(true);
            else blindsDown.setSelected(true);
            blindsGroup.selectedToggleProperty().addListener(blindsListener);
        }
		if(state)return new MMResponse("Opened blinds in room "+roomName);
		else return new MMResponse("Closed blinds in room "+roomName);
	}

	@Override
	public MMResponse addRoom(String roomName) {
		rooms.add(roomName);
		Room r = RoomModel.getInstance().getRooms().get(rooms.size()-1);
		r.setOnMouseClicked(new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent event) {
				if(selectedRoom!=r.getName()){
					selectedRoom = r.getName();
					r.setSelected(true);
					control.addInputEvent(cmdType.SWITCH_ROOM, new ArrayList<>(Arrays.asList(r.getName())));
				}
			}
	    });
		roomPane.getChildren().add(r);
		selectRoom(roomName);
		return new MMResponse("Added room "+roomName);
	}
    
	@Override
	public MMResponse deleteRoom(String roomName) {
		for(int i=0;i<roomPane.getChildren().size();i++){
			Room r = (Room) roomPane.getChildren().get(i);
			if(r.getName()==roomName){
				roomPane.getChildren().remove(i);
				rooms.remove(i);
				break;
			}	
		}
		
//		if(rooms.size()>0)selectRoom(rooms.get(0));
		
		//TODO:Select different room
		return new MMResponse("Deleted room "+roomName);
	}
	
	public class TemperatureChangeListener implements ChangeListener<Number>{
		@Override
		public void changed(ObservableValue<? extends Number> observable,
				Number oldValue, Number newValue) {
			if (oldValue.intValue()!=newValue.intValue()&&!sTemperature.isValueChanging()) {getLogger().info("Observable: "+observable.getValue().intValue()+" OldValue: "+oldValue.intValue()+" New Value: "+newValue.intValue());
				int i =  newValue.intValue();
				control.addInputEvent(cmdType.CHANGE_TEMP, new ArrayList<>(Arrays.asList(selectedRoom,i)));	
			}
		}
		
	}
	public class BlindsChangeListener implements ChangeListener<Toggle>{
		@Override
		public void changed(ObservableValue<? extends Toggle> observable, Toggle oldValue, Toggle newValue) {
			if(blindsGroup.getSelectedToggle()!=null) {
				RadioButton r = (RadioButton) newValue;
				if(r.getText()=="Up") {
					control.addInputEvent(cmdType.CHANGE_BLIND_STATE, new ArrayList<>(Arrays.asList(selectedRoom,true)));
				}else if(r.getText()=="Down") {
					control.addInputEvent(cmdType.CHANGE_BLIND_STATE, new ArrayList<>(Arrays.asList(selectedRoom,false)));
				}
			}
		}
	}
	public class LightChangeListener implements ChangeListener<Toggle>{
		@Override
		public void changed(ObservableValue<? extends Toggle> observable, Toggle oldValue, Toggle newValue) {
			if(lightGroup.getSelectedToggle()!=null) {
				RadioButton r = (RadioButton) newValue;
				if(r.getText()=="On") {
					control.addInputEvent(cmdType.CHANGE_LIGHT_STATE, new ArrayList<>(Arrays.asList(selectedRoom,true)));
				}else if(r.getText()=="Off") {
					control.addInputEvent(cmdType.CHANGE_LIGHT_STATE, new ArrayList<>(Arrays.asList(selectedRoom,false)));
				}
			}
		}
	}
	
	@Override
	public MMResponse switchLight() {
		if(LightModel.getInstance().getOverheadLightForRoomName(selectedRoom))lightOn.setDisable(false);
    	else lightOff.setDisable(true);
		return new MMResponse("Light in room  "+selectedRoom+" was switched");
		
	}
	
}