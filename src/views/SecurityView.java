package views;

import java.awt.Toolkit;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;

import event.MMCmdInterface;
import event.MMCmdType.cmdType;
import event.MMResponse;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.Label;
import javafx.scene.control.Toggle;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import main.Control;
import models.*;
import util.Loggable;

public class SecurityView extends VBox implements MMCmdInterface,Loggable{
	private Control control;
	private final Image iGarageOpen = new Image(getClass().getResourceAsStream("/garage_open.png"));
	private final Image iGarageClosed = new Image(getClass().getResourceAsStream("/garage_closed.png"));
	private final Image iSecurityOn= new Image(getClass().getResourceAsStream("/security_on.png"));
	private final Image iSecurityOff = new Image(getClass().getResourceAsStream("/security_off.png"));
	private final Image iFireAlarmOn= new Image(getClass().getResourceAsStream("/smoke_alarm_on.png"));
	private final Image iFireAlarmOff = new Image(getClass().getResourceAsStream("/smoke_alarm_off.png"));
	private final Image iCameraOn= new Image(getClass().getResourceAsStream("/camera_on.png"));
	private final Image iCameraOff = new Image(getClass().getResourceAsStream("/camera_off.png"));
	private ToggleGroup cameraGroup,garageGroup,fireAlarmGroup,intrusionGroup;
	ToggleButton fireAlarmOn, fireAlarmOff, garageOpen, garageClose,cameraOn, cameraOff,intrusionOn,intrusionOff;
	
	private ChangeListener<Toggle> smokeAlarmListener = new ChangeListener<Toggle>() {
    	@Override public void changed(ObservableValue<? extends Toggle> observable, Toggle oldValue, Toggle selectedToggle) {
    		if(oldValue!=selectedToggle){
    			String text = ((ToggleButton) selectedToggle).getText();
    			if(selectedToggle!=null) {
    				if(text=="On") {
    					control.addInputEvent(cmdType.SWITCH_SMOKE_ALARM, new ArrayList<Object>(Arrays.asList(true)));
    				}else if(text=="Off"){
    					control.addInputEvent(cmdType.SWITCH_SMOKE_ALARM, new ArrayList<Object>(Arrays.asList(false)));
    				}
    			}
    		}
    	}
    };
    private ChangeListener<Toggle> garageListener = new ChangeListener<Toggle>() {
        @Override public void changed(ObservableValue<? extends Toggle> observable, Toggle oldValue, Toggle selectedToggle) {
        	if(oldValue!=selectedToggle){
        		String text = ((ToggleButton) selectedToggle).getText();
            	if(selectedToggle!=null) {
                    if(text=="Open") {
                    	control.addInputEvent(cmdType.SWITCH_GARAGE, new ArrayList<Object>(Arrays.asList(true)));
                    }else if(text=="Close"){
                    	control.addInputEvent(cmdType.SWITCH_GARAGE, new ArrayList<Object>(Arrays.asList(false)));
                    }
                }
        	}
        }
    };
    private ChangeListener<Toggle> surveillanceListener = new ChangeListener<Toggle>() {
        @Override public void changed(ObservableValue<? extends Toggle> observable, Toggle oldValue, Toggle selectedToggle) {
        	if(oldValue!=selectedToggle){
        		String text = ((ToggleButton) selectedToggle).getText();
            	if(selectedToggle!=null) {
                    if(text=="On") {
                    	control.addInputEvent(cmdType.SWITCH_SURVEILANCE, new ArrayList<Object>(Arrays.asList(true)));
                    }else if(text=="Off"){
                    	control.addInputEvent(cmdType.SWITCH_SURVEILANCE, new ArrayList<Object>(Arrays.asList(false)));
                    }
                }
        	}
        }
    };
    private ChangeListener<Toggle> intrusionListener =new ChangeListener<Toggle>() {
    	@Override public void changed(ObservableValue<? extends Toggle> observable, Toggle oldValue, Toggle selectedToggle) {
    		if(oldValue!=selectedToggle){  
    			String text = ((ToggleButton) selectedToggle).getText();
    			if(selectedToggle!=null) {
    				if(text=="On") {
    					control.addInputEvent(cmdType.SWITCH_INTRUSION, new ArrayList<Object>(Arrays.asList(true)));
    				}else if(text=="Off"){
    					control.addInputEvent(cmdType.SWITCH_INTRUSION, new ArrayList<Object>(Arrays.asList(false)));
    				}
    			}
    		}
    	}
    };
	private Label intrusionLabel,garageLabel,fireAlarmLabel,cameraLabel;
	
	public String getImage(final String pathAndFileName) {
	    final URL url = Thread.currentThread().getContextClassLoader().getResource(pathAndFileName);
	    return url.getPath();
	}
	public SecurityView(Control c, Color color){
		this.control = c;
		setMinSize(600, 400);
		getStylesheets().add("view.css");
		setId("security");
		HBox garageWrapper = new HBox();
		garageWrapper.setId("securitybox");
		ImageView garageImage;
		if(SecurityModel.getInstance().isGarage())garageImage = new ImageView(iGarageOpen);
		else garageImage = new ImageView(iGarageClosed);
        garageLabel = new Label("Garage", garageImage);
        garageLabel.setContentDisplay(ContentDisplay.LEFT);
		garageOpen = new ToggleButton("Open");
		garageClose = new ToggleButton("Close");
		garageGroup = new ToggleGroup();
        garageOpen.setToggleGroup(garageGroup);
        garageClose.setToggleGroup(garageGroup);
        if(SecurityModel.getInstance().isGarage())garageOpen.setSelected(true);
		else garageClose.setSelected(true);
        garageWrapper.getChildren().addAll(garageLabel,garageOpen,garageClose);
        this.getChildren().add(garageWrapper);
        garageGroup.selectedToggleProperty().addListener(garageListener);
        HBox cameraWrapper = new HBox();
        cameraWrapper.setId("securitybox");
        ImageView cameraImage;
        if(SecurityModel.getInstance().isSurveilance())cameraImage = new ImageView(iCameraOn);
		else cameraImage = new ImageView(iCameraOff);
        cameraLabel = new Label("Surveilance", cameraImage);
		cameraOn= new ToggleButton("On");
		cameraOff = new ToggleButton("Off");
		cameraGroup = new ToggleGroup();
        cameraOn.setToggleGroup(cameraGroup);
        cameraOff.setToggleGroup(cameraGroup);
        if(SecurityModel.getInstance().isSurveilance())cameraOn.setSelected(true);
        else cameraOff.setSelected(true);
        cameraWrapper.getChildren().addAll(cameraLabel,cameraOn,cameraOff);
        this.getChildren().add(cameraWrapper);
        cameraGroup.selectedToggleProperty().addListener(surveillanceListener);
        HBox intrusionWrapper = new HBox();
        intrusionWrapper.setId("securitybox");
        ImageView intrusionImage;
        if(SecurityModel.getInstance().isIntrusionAlarm())intrusionImage = new ImageView(iSecurityOn);
		else intrusionImage = new ImageView(iSecurityOff);
        intrusionLabel = new Label("Intrusion Detection", intrusionImage);
        intrusionOn= new ToggleButton("On");
        intrusionOff = new ToggleButton("Off");
        intrusionGroup = new ToggleGroup();
        intrusionOn.setToggleGroup(intrusionGroup);
        intrusionOff.setToggleGroup(intrusionGroup);
        if(SecurityModel.getInstance().isIntrusionAlarm())intrusionOn.setSelected(true);
        else intrusionOff.setSelected(true);
        intrusionWrapper.getChildren().addAll(intrusionLabel,intrusionOn,intrusionOff);
        this.getChildren().add(intrusionWrapper);
        intrusionGroup.selectedToggleProperty().addListener(intrusionListener);
        
        HBox fireAlarmWrapper = new HBox();
        fireAlarmWrapper.setId("securitybox");
        ImageView fireAlarmImage;
        if(SecurityModel.getInstance().isSmokeAlarm())fireAlarmImage = new ImageView(iFireAlarmOn);
		else fireAlarmImage = new ImageView(iFireAlarmOff);
        fireAlarmLabel = new Label("Smoke Alarm", fireAlarmImage);
        fireAlarmOn= new ToggleButton("On");
//		on.setId(String.valueOf(socket_id));
        fireAlarmOff = new ToggleButton("Off");
//		off.setId(String.valueOf(socket_id));
        fireAlarmGroup = new ToggleGroup();
        fireAlarmOn.setToggleGroup(fireAlarmGroup);
        fireAlarmOff.setToggleGroup(fireAlarmGroup);
        if(SecurityModel.getInstance().isSmokeAlarm())fireAlarmOn.setSelected(true);
        else fireAlarmOff.setSelected(true);
        fireAlarmWrapper.getChildren().addAll(fireAlarmLabel,fireAlarmOn,fireAlarmOff);
        this.getChildren().add(fireAlarmWrapper);
        fireAlarmGroup.selectedToggleProperty().addListener(smokeAlarmListener);
	}

	@Override
	public MMResponse switchGarage(boolean garage) {
		garageGroup.selectedToggleProperty().removeListener(garageListener);
		if(garage) {
			garageLabel.setGraphic(new ImageView(iGarageOpen));
			garageOpen.setSelected(true);
			garageGroup.selectedToggleProperty().addListener(garageListener);
			return new MMResponse("Garage was opened");
		}else {
			garageLabel.setGraphic(new ImageView(iGarageClosed));
			garageClose.setSelected(true);
			garageGroup.selectedToggleProperty().addListener(garageListener);
			return new MMResponse("Garage was closed");
		}
	}

	@Override
	public MMResponse switchSurveilance(boolean surveilance) {
		cameraGroup.selectedToggleProperty().addListener(surveillanceListener);
		if(surveilance) {
			cameraLabel.setGraphic(new ImageView(iCameraOn));
			cameraOn.setSelected(true);
			cameraGroup.selectedToggleProperty().removeListener(surveillanceListener);
			return new MMResponse("Camera surveilance was activated");
		}else {
			cameraLabel.setGraphic(new ImageView(iCameraOff));
			cameraOff.setSelected(true);
			cameraGroup.selectedToggleProperty().addListener(surveillanceListener);
			return new MMResponse("Camera surveilance was deactivated");
		}
	}

	@Override
	public MMResponse switchIntrusionDetection(boolean intrusionDetection) {
		intrusionGroup.selectedToggleProperty().removeListener(intrusionListener);
		if(intrusionDetection) {
			intrusionLabel.setGraphic(new ImageView(iSecurityOn));
			intrusionOn.setSelected(true);
			intrusionGroup.selectedToggleProperty().addListener(intrusionListener);
			return new MMResponse("Intrusion detection was activated");
		}else {
			intrusionLabel.setGraphic(new ImageView(iSecurityOff));
			intrusionOff.setSelected(true);
			intrusionGroup.selectedToggleProperty().addListener(intrusionListener);
			return new MMResponse("Intrusion detection was deactivated");
		}
	}

	@Override
	public MMResponse switchSmokeAlarm(boolean smokeAlarm) {
		fireAlarmGroup.selectedToggleProperty().removeListener(smokeAlarmListener);
		if(smokeAlarm) {
			fireAlarmLabel.setGraphic(new ImageView(iFireAlarmOn));
			fireAlarmOn.setSelected(true);
			fireAlarmGroup.selectedToggleProperty().addListener(smokeAlarmListener);
			return new MMResponse("Smoke alarm was activated");
		}else {
			fireAlarmLabel.setGraphic(new ImageView(iFireAlarmOff));
			fireAlarmOff.setSelected(true);
			fireAlarmGroup.selectedToggleProperty().addListener(smokeAlarmListener);
			return new MMResponse("Smokae alarm was deactivated");
		}
	}
	
	
	
}
