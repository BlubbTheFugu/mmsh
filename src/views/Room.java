package views;

import models.RoomModel;
import util.Loggable;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Cursor;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.StackPane;
import javafx.scene.text.Text;

public class Room extends StackPane implements Loggable {
	private boolean selected = false;
	private String name;
	private double x, y;
	private boolean light;
	private int width, height;

	public boolean isSelected() {
		return selected;
	}

	public void setSelected(boolean selected) {
		if (selected)
			this.setId("roomSelected");
		else
			this.setId("roomUnselected");
		this.selected = selected;
	}

	public double getX() {
		return x;
	}

	public void setX(double x) {
		this.x = x;
	}

	public double getY() {
		return y;
	}

	public void setY(double y) {
		this.y = y;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setWidth(int width) {
		this.width = width;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	public Room(String name, int xStart,int  yStart, int width, int height, boolean light) {
		super();
		this.name = name;
		this.getChildren().add(new Text(name));
		this.setTranslateX(x);
		this.setTranslateY(y);
		this.setPrefSize(width, height);
		this.setCursor(Cursor.HAND);
		this.setId("roomUnselected");
		getStylesheets().add("room.css");
		this.x = xStart;
		this.y = yStart;
		this.light = light;
		setLayoutX(xStart);
		setLayoutY(yStart);
		this.width = width;
		this.height = height;
		final Delta dragDelta = new Delta();
		setOnMousePressed(new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent mouseEvent) {
				// record a delta distance for the drag and drop operation.
				dragDelta.x = getLayoutX() - mouseEvent.getSceneX();
				dragDelta.y = getLayoutY() - mouseEvent.getSceneY();
				setCursor(Cursor.MOVE);
			}
		});
		setOnMouseReleased(new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent mouseEvent) {
				RoomModel.getInstance().changeRoomPosition((int)x,(int) y, getName());
				setCursor(Cursor.HAND);
			}
		});
		setOnMouseDragged(new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent mouseEvent) {
				Room.this.x =  (mouseEvent.getSceneX() + dragDelta.x);
				Room.this.y = (mouseEvent.getSceneY() + dragDelta.y);
				if (x > 0 && x < 300)
					setLayoutX(x);
				if (y > 0 && y < 300)
					setLayoutY(y);
				getLogger().info(getLayoutX() + " " + getLayoutY());

			}
		});
		setOnMouseEntered(new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent mouseEvent) {
				setCursor(Cursor.HAND);
			}
		});
	}

	class Delta {
		double x, y;
	}
	// allow the label to be dragged around.

}
