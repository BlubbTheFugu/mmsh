package views;

import java.util.ArrayList;
import java.util.Arrays;

import de.jensd.fx.glyphs.GlyphsDude;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIcon;
import event.MMCmdInterface;
import event.MMCmdType.cmdType;
import event.MMResponse;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.geometry.Side;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.Separator;
import javafx.scene.control.Slider;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.Toggle;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.ToolBar;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import main.Control;
import main.Control.model;
import modality.Modality.response;
import models.*;

public class PowerView extends BorderPane implements MMCmdInterface{
	private Control control;
	
	public PowerView(Control c, Color color){
//		super(c, color, new StackPane());
		this.control = c;
		this.getStylesheets().add("view.css");
        final TabPane tabPane = new TabPane();
        tabPane.setPrefSize(400, 400);
        tabPane.setSide(Side.TOP);
        tabPane.setTabClosingPolicy(TabPane.TabClosingPolicy.UNAVAILABLE);
        ObservableList<String> name = FXCollections.observableArrayList(((RoomModel)control.getModel(model.ROOM)).getRoomNames());
        int socket_id = 0;
        for(int i=0;i<name.size();i++){
        	final Tab tab = new Tab();
        	tab.setId("powerTab");
        	tab.setText(name.get(i));
        	tabPane.getTabs().add(tab);
        	ArrayList<String> socket_names = ((PowerModel)control.getModel(model.POWER)).getDescriptionForRoomName(name.get(i));
        	ArrayList<Boolean> activated = ((PowerModel)control.getModel(model.POWER)).getActivated();
        	VBox wrapper = new VBox();
        	if(socket_names!=null){
        		for(String sName:socket_names){
            		HBox socketWrapper = new HBox();
            		socketWrapper.setId("socketWrapper");
            		Text label = new Text(sName);
            		ToggleButton on = new ToggleButton("On");
            		on.setId(String.valueOf(socket_id));
            		ToggleButton off = new ToggleButton("Off");
            		off.setId(String.valueOf(socket_id));
            		ToggleGroup group = new ToggleGroup();
                    on.setToggleGroup(group);
                    off.setToggleGroup(group);
                    if(activated.get(socket_id))on.setSelected(true);
                    else off.setSelected(true);
                    socketWrapper.getChildren().addAll(label,on,off);
                    wrapper.getChildren().add(socketWrapper);
                    group.selectedToggleProperty().addListener(new ChangeListener<Toggle>() {
                        @Override public void changed(ObservableValue<? extends Toggle> observable, Toggle oldValue, Toggle selectedToggle) {
                        	if(oldValue!=selectedToggle){
                        		//TODO: double click on one button fix
                        		String text = ((ToggleButton) selectedToggle).getText();
                            	int id = Integer.valueOf(((ToggleButton) selectedToggle).getId());
                            	if(selectedToggle!=null) {
                                    if(text=="On") {
                                    	control.addInputEvent(cmdType.TURN_SOCKET_ON, new ArrayList<Object>(Arrays.asList(id)));
                                    }else if(text=="Off"){
                                    	control.addInputEvent(cmdType.TURN_SOCKET_OFF, new ArrayList<Object>(Arrays.asList(id)));
                                    }
                                }
                        	}
                        }
                    });
            		socket_id++;
            	}
        	}
        	tab.setContent(wrapper);
        }
        this.setCenter(tabPane);
        this.setRight(rightToolbar());
	}
	private ToolBar rightToolbar(){
        ToolBar t = new ToolBar();
        t.setPadding(new Insets(10,10,10,10));
        Label plugLabel = GlyphsDude.createIconLabel(FontAwesomeIcon.PLUG,"Plugs","15px;","13px",ContentDisplay.LEFT); 

        Button bTurnAllOn = GlyphsDude.createIconButton(FontAwesomeIcon.BATTERY_FULL,"Turn all on","15px;","13px",ContentDisplay.LEFT);
        bTurnAllOn.setOnAction((event) -> {
//          control.addInputEvent(cmdType.ADD_PLAYLIST, new ArrayList<>());
        });
        Button bTurnAllOff = GlyphsDude.createIconButton(FontAwesomeIcon.BATTERY_EMPTY,"Turn all off","15px;","13px",ContentDisplay.LEFT);
        bTurnAllOff.setOnAction((event) -> {
//          control.addInputEvent(cmdType.ADD_PLAYLIST, new ArrayList<>());
        });
        Button bTurnRoomOn = GlyphsDude.createIconButton(FontAwesomeIcon.PLUS,"Turn room on","15px;","13px",ContentDisplay.LEFT);
        bTurnRoomOn.setOnAction((event) -> {
//          control.addInputEvent(cmdType.ADD_PLAYLIST, new ArrayList<>());
        });
        Button bTurnRoomOff = GlyphsDude.createIconButton(FontAwesomeIcon.PLUS,"Turn room off","15px;","13px",ContentDisplay.LEFT);
        bTurnRoomOff.setOnAction((event) -> {
//          control.addInputEvent(cmdType.ADD_PLAYLIST, new ArrayList<>());
        });
        Button bAddPowerSocket = GlyphsDude.createIconButton(FontAwesomeIcon.PLUS,"Add socket","15px;","13px",ContentDisplay.LEFT);
        bAddPowerSocket.setOnAction((event) -> {
//          control.addInputEvent(cmdType.ADD_PLAYLIST, new ArrayList<>());
        });
        Button bDeletePowerSocket = GlyphsDude.createIconButton(FontAwesomeIcon.MINUS,"Remove socket","15px;","13px",ContentDisplay.LEFT);
        bDeletePowerSocket.setOnAction((event) -> {
//          control.addInputEvent(cmdType.ADD_PLAYLIST, new ArrayList<>());
        });
        t.getItems().addAll(plugLabel,bTurnAllOn,bTurnAllOff,bTurnRoomOn,bTurnRoomOff,bAddPowerSocket,bDeletePowerSocket);
        t.setOrientation(Orientation.VERTICAL);
        return t;
    }

	@Override
	public MMResponse turnSocketOn(int socketID) {
		
		return new MMResponse("Turned socket "+socketID+" on");
	}

	@Override
	public MMResponse turnSocketOff(int socketID) {
		
		return new MMResponse("Turned socketd "+socketID+" off");
	}
	
}
