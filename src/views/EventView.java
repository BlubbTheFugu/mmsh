package views;

import java.util.ArrayList;
import java.util.Map;

import event.MMCmdInterface;
import event.MMResponse;
import javafx.collections.FXCollections;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.ToolBar;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.BorderPane;
import javafx.scene.paint.Color;
import main.Control;
import main.Control.model;
import models.EventModel;
import util.Loggable;
import util.LoggedEvent;

public class EventView extends BorderPane implements Loggable,MMCmdInterface{
	
	private Control control;
	private ToolBar toolbar;
	private TableView<LoggedEvent> eventTable;
	
	public EventView(Control c,Color color){
		this.control = c;
		setMinSize(600, 400);
		getStylesheets().add("view.css");
		setId("view");
		this.update();
// 		this.setRight(rightToolbar());
	}
	
    private ToolBar createToolBar() {
	    ToolBar toolbar = new ToolBar();
		
// 		toolbar.getItems().addAll();
 		return toolbar;
    }
      
    private ToolBar rightToolbar(){
    	ToolBar t = new ToolBar();
    	
// 		t.getItems().addAll(sTemperature,lightSourceLabel,cLightSource);
// 		t.setOrientation(Orientation.VERTICAL);
// 		this.getChildren().add(toolbar);
 		return t;
    }
	
	private TableView<LoggedEvent> loadEventTable(){
		TableView<LoggedEvent> table = createEventTable();
		ArrayList<LoggedEvent> eventList = ((EventModel)control.getModel(model.EVENT_LOG)).getEvents();
		if(eventList!=null&&!eventList.isEmpty())table.setItems(FXCollections.observableArrayList(eventList));
		else table.setItems(FXCollections.observableArrayList());
		return table;
	}
	
	private TableView<LoggedEvent> createEventTable(){
		TableView<LoggedEvent> table = new TableView<LoggedEvent>();
		TableColumn<LoggedEvent, String> type = new TableColumn<LoggedEvent, String>();
		type.setText("Type");
		type.setCellValueFactory(new PropertyValueFactory<LoggedEvent, String>("type"));
		TableColumn<LoggedEvent, String> destination = new TableColumn<LoggedEvent, String>();
		destination.setText("Destination");
		destination.setCellValueFactory(new PropertyValueFactory<LoggedEvent, String>("destination"));
		TableColumn<LoggedEvent, String> modality = new TableColumn<LoggedEvent, String>();
		modality.setText("Modality");
		modality.setCellValueFactory(new PropertyValueFactory<LoggedEvent, String>("mod"));
		TableColumn<LoggedEvent, String> context = new TableColumn<LoggedEvent, String>();
		context.setText("Context");
		context.setCellValueFactory(new PropertyValueFactory<LoggedEvent, String>("context"));
		TableColumn<LoggedEvent, String> responseMessage = new TableColumn<LoggedEvent, String>();
		responseMessage.setText("Response");
		responseMessage.setCellValueFactory(new PropertyValueFactory<LoggedEvent, String>("responseMessage"));
		TableColumn<LoggedEvent, String> status = new TableColumn<LoggedEvent, String>();
		status.setText("Status");
		status.setCellValueFactory(new PropertyValueFactory<LoggedEvent, String>("status"));		
		table.getColumns().addAll(type,destination,modality,context,responseMessage,status);
		return table;
	}
	
	@Override
	public MMResponse updateView() {
		eventTable = loadEventTable();	
		this.setCenter(eventTable);
		return MMCmdInterface.super.updateView();
	}

	public void update(){
		eventTable = loadEventTable();	
		this.setCenter(eventTable);
	}
}