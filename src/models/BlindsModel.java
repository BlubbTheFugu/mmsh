package models;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

import database.DBAdapter;
import main.ModelInterface;

public class BlindsModel extends DBAdapter implements ModelInterface {
	
	private static BlindsModel instance;
	private HashMap<String,Boolean> blindsForRoom;
	private ArrayList<Boolean> blinds;
	
	public boolean getBlindsForRoom(String roomName) {
		return blindsForRoom.get(roomName);
	}
	
	public boolean getBlindsForId(int id) {
		return blinds.get(id);
	}
	
	public void changeBlindsState(String roomName, boolean state) {
		String query;
		if(state) query = "UPDATE room SET blinds=1 WHERE name='"+roomName+"';";
		else query = "UPDATE room SET blinds=0 WHERE name='"+roomName+"';";
		this.updateDB(query);
		blindsForRoom.put(roomName,state);
//		blinds.set(, arg1))
	}
	
	public static BlindsModel getInstance () {
	    if (BlindsModel.instance == null) {
	    	BlindsModel.instance = new BlindsModel ();
	    }
	    return BlindsModel.instance;
	}
	
	public BlindsModel() {
		super();
		update();
	}

	@Override
	public void update() {
		ResultSet rs = getResult("SELECT r.room_id,r.name,r.blinds FROM room r");
		blindsForRoom = new HashMap<String,Boolean>();
		blinds = new ArrayList<Boolean>();
		try {
			while(rs.next()){
				boolean blindsValue;
				if(rs.getInt("blinds")==0)blindsValue = false;
				else blindsValue = true;
				blindsForRoom.put(rs.getString("name"),blindsValue);
				blinds.add(blindsValue);
			}
			rs.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}		
	}	
}
