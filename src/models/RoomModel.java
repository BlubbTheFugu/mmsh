package models;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import views.Room;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import database.DBAdapter;
import database.DBConnection;
import event.MMCmdInterface;
import main.ModelInterface;
import modality.Modality.response;

public class RoomModel extends DBAdapter implements ModelInterface {
	
	private static RoomModel instance;
	
	private ArrayList<String> roomNames;
	private ArrayList<Room> roomShapes;
	
	public ArrayList<String> getRoomNames() {
		return roomNames;
	}
	
	public ArrayList<Room> getRooms(){
		return roomShapes;
	}

	public static RoomModel getInstance () {
	    if (RoomModel.instance == null) {
	    	RoomModel.instance = new RoomModel ();
	    }
	    return RoomModel.instance;
	}
	
	public void addRoom(String roomName){
		String query = "INSERT INTO room (name,width,height,temperature) VALUES ('"+roomName+"',20,20,20.0);";
		this.updateDB(query);
		update();
	}
	
	public void deleteRoom(String roomName){
		String query = "DELETE FROM room WHERE room.name = '"+roomName+"';";
		this.execute(query);
		update();
	}
	
	public void newLayout(){
		String query = "DELETE FROM room;";
		this.execute(query);
		update();
	}
	
	public RoomModel() {
		update();
	}
	
	public void changeRoomPosition(int x, int y, String roomName){
		String query = "UPDATE room SET x="+x+",y="+y+" WHERE name='"+roomName+"';";
		this.updateDB(query);
		update();
	}
	
	@Override
	public void update() {
		ResultSet rs = getResult("select * from room");
		roomNames = new ArrayList<String>();
		roomShapes = new ArrayList<Room>();
		try {
			while(rs.next()){
				roomNames.add(rs.getString("name"));
				boolean light;
				if(rs.getInt("overhead_light")==0)light = false;
				else light = true;
				Room r = new Room(rs.getString("name"),rs.getInt("x"), rs.getInt("y"), rs.getInt("width"), rs.getInt("height"), light );	
				roomShapes.add(r);
			}
			rs.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}		
	}	
}
