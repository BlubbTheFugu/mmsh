package models;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

import database.DBAdapter;
import database.DBConnection;
import main.ModelInterface;

public class PowerModel extends DBAdapter implements ModelInterface {
	
	private static PowerModel instance;
	
	private ArrayList<Boolean> activated;
	private HashMap<String,ArrayList<String>> powerSockets;
	
	public static PowerModel getInstance () {
	    if (PowerModel.instance == null) {
	    	PowerModel.instance = new PowerModel ();
	    }
	    return PowerModel.instance;
	}
	
	public PowerModel() {
		super();
		update();
	}
	
	public ArrayList<String> getDescriptionForRoomName(String roomName){
		ArrayList<String> socketName = powerSockets.get(roomName);
		return socketName;
	}
	
	public ArrayList<Boolean> getActivated() {
		return activated;
	}

	@Override
	public void update() {
		ResultSet rs = getResult("SELECT p.socket_id,r.name,p.description,p.activated FROM power_socket p LEFT JOIN room r WHERE p.fka_room_id = r.room_id");
		powerSockets = new HashMap<String,ArrayList<String>>();
		activated = new ArrayList<Boolean>();
		try {
			while(rs.next()){
				String roomName = rs.getString("name");
				String description = rs.getString("description");
				int activatedSocket = rs.getInt("activated");
				int socketId = rs.getInt("socket_id");
				//getLogger().info(roomName);
				//getLogger().info(description);
				if(powerSockets.get(roomName)==null)powerSockets.put(roomName, new ArrayList<String>());
				ArrayList<String> list = powerSockets.get(roomName);
				list.add(description);
				powerSockets.put(roomName,list);
				
				if(activatedSocket==0)activated.add(false);
				else activated.add(true);
//				int light_id = rs.getInteger("light_id");
			}
			
			rs.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}		
	}


	public void turnSocketOn(int socketID) {
		String query = "UPDATE power_socket SET activated=1 WHERE socket_id="+socketID+";";
		this.updateDB(query);
	}	
	
	public void turnSocketOff(int socketID) {
		String query = "UPDATE power_socket SET activated=0 WHERE socket_id="+socketID+";";
		this.updateDB(query);
	}	
}
