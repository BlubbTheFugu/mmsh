package models;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import util.Song;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import database.DBAdapter;
import database.DBConnection;
import main.ModelInterface;

public class EntertainmentModel extends DBAdapter implements ModelInterface {
	
	private static EntertainmentModel instance;
	
	private ArrayList<String> playlistNames;
	private Map<Integer,ArrayList<Song>> songs;
	
	public ArrayList<String> getPlaylistNames() {
		return playlistNames;
	}

	public static EntertainmentModel getInstance () {
	    if (EntertainmentModel.instance == null) {
	    	EntertainmentModel.instance = new EntertainmentModel ();
	    }
	    return EntertainmentModel.instance;
	}
	
	public EntertainmentModel() {
		update();
	}

	public ArrayList<Song> getSongForPlaylistID(int playlist_id){
		return songs.get(playlist_id);
	}

	public void addSongToPlaylist(int playlist,Song s){
		String query = "INSERT INTO song(fka_playlist_id,title,artist,album,duration,path) VALUES ("+playlist+",'"+s.getTitle()+"','"+s.getArtist()+"','"+s.getAlbum()+"','"+s.getMillis()+"','"+s.getPath()+"');";
		ArrayList<Song> added = songs.get(playlist);
		added.add(s);
		songs.put(playlist, added);
		this.updateDB(query);
	}

	public void addPlaylist(String playlistName){
		String query = "INSERT INTO playlist(playlist_name) VALUES ('"+playlistName+"');";
		songs.put(playlistNames.size(), new ArrayList<Song>());
		playlistNames.add(playlistName);
		this.updateDB(query);
	}
	
	public void deletePlaylist(String playlistName){
		String query = "DELETE FROM playlist WHERE playlist.playlist_name = '"+playlistName+"';";
		songs.remove(playlistNames.indexOf(playlistName));
		playlistNames.remove(playlistName);
		this.execute(query);
	}
	
	@Override
	public void update() {
		playlistNames = new ArrayList<String>();
		songs = new HashMap<Integer,ArrayList<Song>>();
		try {
			ResultSet pl = getResult("SELECT * FROM playlist;");
			while(pl.next()){
				String playlistName = pl.getString("playlist_name");
				playlistNames.add(playlistName);
			}
			pl.close();
			for(int i=0;i<playlistNames.size();i++)songs.put(i,new ArrayList<Song>());
			ResultSet rs = getResult("SELECT * FROM song s;");
			while(rs.next()){
				String title = rs.getString("title");
				String artist = rs.getString("artist");
				String album = rs.getString("album");
				int duration = rs.getInt("duration");
				String path = rs.getString("path");
				int playlist = rs.getInt("fka_playlist_id");
				Song s = new Song(title,artist,album,duration,path);
				ArrayList<Song> newPlaylist = songs.get(playlist);
				newPlaylist.add(s);
				songs.put(playlist, newPlaylist);		
			}
			rs.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}		
	}	
}
