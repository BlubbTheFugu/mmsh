package models;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

import database.DBAdapter;
import database.DBConnection;
import event.MMCmdInterface;
import main.ModelInterface;
import modality.Modality.response;

public class TemperatureModel extends DBAdapter implements ModelInterface {
	
	private static TemperatureModel instance;
	private HashMap<String,Integer> tempForRoom;
	private ArrayList<Integer> temp;
	
	public Integer getTempForRoom(String roomName) {
		return tempForRoom.get(roomName);
	}
	
	public Integer getTempForId(int id) {
		return temp.get(id);
	}
	
	public void changeTemperature(String roomName, int tmp) {
		String query = "UPDATE room SET temperature="+tmp+" WHERE name='"+roomName+"';";
		this.updateDB(query);
	}
	
	public static TemperatureModel getInstance () {
	    if (TemperatureModel.instance == null) {
	    	TemperatureModel.instance = new TemperatureModel ();
	    }
	    return TemperatureModel.instance;
	}
	
	public TemperatureModel() {
		super();
		update();
	}

	@Override
	public void update() {
		ResultSet rs = getResult("SELECT r.room_id,r.name,r.temperature FROM room r");
		tempForRoom = new HashMap<String,Integer>();
		temp = new ArrayList<Integer>();
		try {
			while(rs.next()){
				tempForRoom.put(rs.getString("name"),rs.getInt("temperature"));
				temp.add(rs.getInt("temperature"));
			}
			rs.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}		
	}

}
