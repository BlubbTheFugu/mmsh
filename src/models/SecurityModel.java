package models;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import database.DBAdapter;
import main.ModelInterface;
import util.LoggedEvent;

public class SecurityModel extends DBAdapter implements ModelInterface{
	
	private static SecurityModel instance;
	private boolean garage, surveilance, smokeAlarm, intrusionAlarm;
	
	public boolean isGarage() {
		return garage;
	}

	public void setGarage(boolean garage) {
		String query;
		if(garage)query = "UPDATE security SET activated=1 WHERE device_name='Garage';";
		else query = "UPDATE security SET activated=0 WHERE device_name='Garage';";
		this.updateDB(query);
		this.garage = garage;
	}

	public boolean isSurveilance() {
		return surveilance;
	}

	public void setSurveilance(boolean surveillance) {
		String query;
		if(surveillance)query = "UPDATE security SET activated=1 WHERE device_name='Surveilance';";
		else query = "UPDATE security SET activated=0 WHERE device_name='Surveilance';";
		this.updateDB(query);
		this.surveilance = surveillance;
	}

	public boolean isSmokeAlarm() {
		return smokeAlarm;
	}

	public void setSmokeAlarm(boolean fireAlarm) {
		String query;
		if(fireAlarm)query = "UPDATE security SET activated=1 WHERE device_name='Smoke Alarm';";
		else query = "UPDATE security SET activated=0 WHERE device_name='Smoke Alarm';";
		this.updateDB(query);
		this.smokeAlarm = fireAlarm;
	}

	public boolean isIntrusionAlarm() {
		return intrusionAlarm;
	}

	public void setIntrusionAlarm(boolean intrusionAlarm) {
		String query;
		if(intrusionAlarm)query = "UPDATE security SET activated=1 WHERE device_name='Intrusion Alarm';";
		else query = "UPDATE security SET activated=0 WHERE device_name='Intrusion Alarm';";
		this.updateDB(query);
		this.intrusionAlarm = intrusionAlarm;	
	}

	public static SecurityModel getInstance () {
	    if (SecurityModel.instance == null) {
	    	SecurityModel.instance = new SecurityModel ();
	    }
	    return SecurityModel.instance;
	}
	
	public SecurityModel() {
		super();
		update();
	}
	
	@Override
	public void update() {
		ResultSet rs = getResult("SELECT * FROM security;");
		try {
			while(rs.next()){
				if(rs.getString("device_name").equals("Garage")) {
					if((rs.getInt("activated"))==0)garage = false;
					else garage = true;
				}else if(rs.getString("device_name").equals("Intrusion Alarm")){
					if((rs.getInt("activated"))==0)intrusionAlarm = false;
					else intrusionAlarm = true;
				}else if(rs.getString("device_name").equals("Surveilance")){
					if((rs.getInt("activated"))==0)surveilance = false;
					else surveilance = true;
				}else if(rs.getString("device_name").equals("Smoke Alarm")){
					if((rs.getInt("activated"))==0)smokeAlarm  = false;
					else smokeAlarm  = true;		
				}
			}
			rs.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
}
