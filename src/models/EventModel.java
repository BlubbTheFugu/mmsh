package models;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.apache.log4j.pattern.LogEvent;

import util.LoggedEvent;
import views.Room;
import database.DBAdapter;
import main.ModelInterface;

public class EventModel extends DBAdapter implements ModelInterface {
	
	private static EventModel instance;
	private ArrayList<LoggedEvent> events;
	
	public static EventModel getInstance () {
	    if (EventModel.instance == null) {
	    	EventModel.instance = new EventModel ();
	    }
	    return EventModel.instance;
	}
	
	public EventModel() {
		super();
		update();
	}
	
	public void addEvent(LoggedEvent le){
		events.add(le);
		logEvent(le);
	}
	
	@Override
	public void update() {
		ResultSet rs = getResult("SELECT * FROM event_history");
		events = new ArrayList<LoggedEvent>();
		try {
			while(rs.next()){
				String type = rs.getString("cmd_type");
				String destination = rs.getString("destination");
				String mod = rs.getString("modality");
				String context = rs.getString("context");
				String responseMessage = rs.getString("response");
				String status = rs.getString("status");
				LoggedEvent le = new LoggedEvent(type, context,destination, mod, status, responseMessage);
				events.add(le);
			}
			rs.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public void logEvent(LoggedEvent le){
		String query = "INSERT INTO event_history (cmd_type,destination,modality,context,response,status) VALUES ('"+le.typeProperty().get()+"','"+le.destinationProperty().get()+"','"+le.modProperty().get()+"','"+le.contextProperty().get()+"','"+le.responseMessageProperty().get()+"','"+le.statusProperty().get()+"');";
		this.updateDB(query);
	}

	public ArrayList<LoggedEvent> getEvents() {
		return events;
	}


}
