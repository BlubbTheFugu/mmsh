package models;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

import database.DBAdapter;
import main.ModelInterface;

public class LightModel extends DBAdapter implements ModelInterface {
	
	private static LightModel instance;
	
	private HashMap<String,ArrayList<String>> roomLights;
	private HashMap<String,Boolean> overheadLights;
	
	public static LightModel getInstance () {
	    if (LightModel.instance == null) {
	    	LightModel.instance = new LightModel ();
	    }
	    return LightModel.instance;
	}
	
	public LightModel() {
		update();
	}
	
	public ArrayList<String> getDescriptionForRoomName(String roomName){
		ArrayList<String> description = roomLights.get(roomName);
		if(description==null)description = new ArrayList<String>(Arrays.asList("No light sources"));
		return description;
	}
	
	public boolean getOverheadLightForRoomName(String roomName){
		return overheadLights.get(roomName);
	}
	
	public void changeLightState(String roomName, boolean state) {
        String query;
        if(state) query = "UPDATE room SET overhead_light=1 WHERE name='"+roomName+"';";
        else query = "UPDATE room SET overhead_light=0 WHERE name='"+roomName+"';";
        this.updateDB(query);
        this.update();
    }
	
	public void addLightsource(int roomID,String roomName, String description){
		String query = "INSERT INTO light_source (fka_room_id,cmd_name,description,activated) VALUES ('"+roomID+"','Dummy_Befehl','"+description+"',0);";
		if(roomLights.get(roomName)==null)roomLights.put(roomName, new ArrayList<String>());
		ArrayList<String> list = roomLights.get(roomName);
		list.add(description);
		roomLights.put(roomName,list);
		this.updateDB(query);
	}
	
	@Override
	public void update() {
		ResultSet rs = getResult("SELECT  l.light_id,r.name,r.room_id,r.overhead_light,l.description,l.activated FROM light_source l LEFT JOIN room r WHERE l.fka_room_id = r.room_id");
		roomLights = new HashMap<String,ArrayList<String>>();
		overheadLights = new HashMap<String,Boolean>();
		try {
			while(rs.next()){
				String roomName = rs.getString("name");
				String description = rs.getString("description");
				int room_id = rs.getInt("room_id");
				if(roomLights.get(roomName)==null)roomLights.put(roomName, new ArrayList<String>());
				ArrayList<String> list = roomLights.get(roomName);
				list.add(description);
				boolean light;
				if(rs.getInt("overhead_light")==0)light =false;
				else light = true;
				if(overheadLights.get(roomName)==null)overheadLights.put(roomName, light);
				roomLights.put(roomName,list);
//				int light_id = rs.getInteger("light_id");
			}
			
			rs.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}		
	}

}
