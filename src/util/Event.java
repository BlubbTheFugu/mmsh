package util;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.util.Duration;

public class Event {
	
    private StringProperty title;
    private StringProperty artist;
    private StringProperty album;
    private StringProperty duration;
    private String path;
    private Duration durationType;
    private int millis;
    
    public String getPath() {
		return path;
	}
    
	public void setPath(String path) {
		this.path = path;
	}
	
	public String getTitle() {
		return title.get();
	}

	public String getArtist() {
		return artist.get();
	}

	public String getAlbum() {
		return album.get();
	}

	public String getDuration() {
		return duration.get();
	}

	public int getMillis() {
		return millis;
	}
	
	public Event(String title, String artist, String album, int millis, String path) {
		this.title = new SimpleStringProperty(title);
		this.artist = new SimpleStringProperty(artist);
		this.album = new SimpleStringProperty(album);
		this.millis = millis;
		Duration d = new Duration(millis);
		this.duration = new SimpleStringProperty((int)d.toHours()+":"+(int)(d.toMinutes()%60)+":"+(int)(d.toSeconds()%3600));
		this.path = path;
	}
	
    public StringProperty titleProperty() { return title; }
    public StringProperty artistProperty() { return artist; }
    public StringProperty albumProperty() { return album; }
    public StringProperty durationProperty() { return duration; }

}
