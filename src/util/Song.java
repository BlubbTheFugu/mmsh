package util;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.util.Duration;

public class Song {
	
    private StringProperty title;
    private StringProperty artist;
    private StringProperty album;
    private StringProperty duration;
    private String path;
    private Duration durationType;
    private int millis;
    
    public String getPath() {
		return path;
	}
    
	public void setPath(String path) {
		this.path = path;
	}
	
	public String getTitle() {
		return title.get();
	}

	public String getArtist() {
		return artist.get();
	}

	public String getAlbum() {
		return album.get();
	}

	public String getDuration() {
		return duration.get();
	}

	public int getMillis() {
		return millis;
	}
	
	public Song(String title, String artist, String album, int millis, String path) {
		this.title = new SimpleStringProperty(title);
		this.artist = new SimpleStringProperty(artist);
		this.album = new SimpleStringProperty(album);
		this.millis = millis;
		Duration d = new Duration(millis);
		String hours = String.valueOf(((int)d.toHours()));
		if(hours.length()==1)hours="0"+hours;
		String minutes = String.valueOf(((int)d.toMinutes()%60));
		if(minutes.length()==1)minutes="0"+minutes;
		String seconds = String.valueOf(((int)(d.toSeconds()%60)));
		if(seconds.length()==1)seconds="0"+seconds;		
		this.duration = new SimpleStringProperty(hours+":"+minutes+":"+seconds);
		this.path = path;
	}
	
    public StringProperty titleProperty() { return title; }
    public StringProperty artistProperty() { return artist; }
    public StringProperty albumProperty() { return album; }
    public StringProperty durationProperty() { return duration; }

}
