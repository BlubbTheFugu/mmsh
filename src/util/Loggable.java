package util;

import org.apache.log4j.Logger;

import java.util.HashMap;

public interface Loggable {
    public static HashMap<Class<?>, Logger> logger = new HashMap<Class<?>, Logger>();

    public default Logger getLogger() {
        Logger log = logger.get(getClass());
        if (log == null) {
            log = Logger.getLogger(this.getClass());
            logger.put(getClass(), log);
        }
        return log;
    }
    
}
 