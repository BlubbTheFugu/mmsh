package util;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class XMLParser implements Loggable{
	
	private File xmlFile;
	private Document xmlDoc;
	
	public  XMLParser(String fileName){
		loadDocument(fileName);
	}
	
	public void loadDocument(String fileName){
		try {
			ClassLoader classLoader = getClass().getClassLoader();
			xmlFile = new File(classLoader.getResource(fileName).getFile());
			xmlDoc = readXmlDocument(xmlFile);
		} catch (Exception ex) {
			getLogger().error("Couldn't load DOM");
			ex.printStackTrace();
		}
	}
	
	protected ArrayList<Element> getElementList(String tagName){
		NodeList nodes = xmlDoc.getElementsByTagName(tagName);
		ArrayList<Element> elements = new ArrayList<Element>();
		for (int i = 0; i < nodes.getLength(); i++) {
			Node node = nodes.item(i);		
			if (node.getNodeType() == Node.ELEMENT_NODE) {
				elements.add((Element) node);
			}
		}
		return elements;
	}
			
	protected String getValue(String tag, Element element) {
		NodeList nodes = element.getElementsByTagName(tag).item(0).getChildNodes();
		Node node = (Node) nodes.item(0);
		return node.getNodeValue();
	}
	
	protected boolean getBoolean(String tag, Element element) {
		String value = getValue(tag,element);
		return Boolean.parseBoolean(value);
	}
	
	protected void overwriteAttribute(String tag, int modID, Element old,Element newEle){
		getLogger().info(xmlDoc.getElementsByTagName(tag).item(modID).getNodeName());
		Element root = xmlDoc.getDocumentElement();
//        Element oldMod = (Element)root.get
        root.replaceChild(newEle,old);
	}
	
	private void overwriteXmlFile(File xmlFile, Document document,
	            Transformer transformer) throws FileNotFoundException,
	            TransformerException {
	        StreamResult result = new StreamResult(new PrintWriter(
	                new FileOutputStream(xmlFile, false)));
	        DOMSource source = new DOMSource(document);
	        transformer.transform(source, result);
	}
	
	private Transformer createXmlTransformer() throws Exception {
        Transformer transformer = TransformerFactory.newInstance()
                .newTransformer();
        transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
        transformer.setOutputProperty(OutputKeys.INDENT, "yes");
        return transformer;
    }
	
	private Document readXmlDocument(File xmlFile) throws Exception {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document document = builder.parse(xmlFile);
    	document.getDocumentElement().normalize();
        return document;
    }
	
	protected void saveXMLChanges(){
		TransformerFactory transformerFactory = TransformerFactory.newInstance();
		Transformer transformer = null;
		try {
			transformer = transformerFactory.newTransformer();
			DOMSource source = new DOMSource(xmlDoc);
			StreamResult result = new StreamResult(xmlFile.getAbsolutePath());
			transformer.transform(source, result);
			transformer = transformerFactory.newTransformer();
		} catch (TransformerException e) {
			getLogger().error("Couldn't save xml changes");
			e.printStackTrace();
		}
	}
	
}
