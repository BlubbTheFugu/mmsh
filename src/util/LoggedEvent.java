package util;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class LoggedEvent {

	private StringProperty type,context,destination,mod,status,responseMessage;

	public LoggedEvent(String type,String destination, String context, String mod, String status,
			String responseMessage) {
		super();
		this.type = new SimpleStringProperty(type);
		this.destination = new SimpleStringProperty(destination);
		this.context = new SimpleStringProperty(context);
		this.mod =  new SimpleStringProperty(mod);
		this.status =  new SimpleStringProperty(status);
		this.responseMessage =  new SimpleStringProperty(responseMessage);
	}

	public StringProperty typeProperty() {
		return type;
	}

	public StringProperty contextProperty() {
		return context;
	}

	public StringProperty destinationProperty() {
		return destination;
	}

	public StringProperty modProperty() {
		return mod;
	}

	public StringProperty statusProperty() {
		return status;
	}

	public StringProperty responseMessageProperty() {
		return responseMessage;
	}

}
