package config;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;

import javax.xml.transform.Transformer;

import main.MMInteractionManager;
import modality.Modality;
import modality.ModalityAttrib;
import modality.OutputInterface;
import modality.SimpleOutputModality;
import modality.ThreadedInputModality;
import modality.ThreadedOutputModality;

import org.w3c.dom.Element;

import util.XMLParser;

public class MMConfigParser extends XMLParser {
	
	private static final String configName = "mmconf.xml";
	public static final String inputTag = "input";
	public static final String outputTag = "output";
	
	public MMConfigParser(){
		super(configName);
	}
	
	public ArrayList<Modality> loadInputModalities(MMInteractionManager mmi){
		ArrayList<Modality> input = new ArrayList<Modality>();
		for(Element i:getElementList(inputTag)){
			if(i==null)break;
			ModalityAttrib att = loadAttributes(i);
			Modality m = createInputModFromDef(input.size(),att,mmi);
			if(m!=null)input.add(m);	
		}
		return input;	
	}
	
	public ArrayList<SimpleOutputModality> loadOutputModalities(MMInteractionManager mmi){
		ArrayList<SimpleOutputModality> output = new ArrayList<SimpleOutputModality>();
		for(Element i:getElementList(outputTag)){
			if(i==null)break;
			ModalityAttrib att = loadAttributes(i);
			SimpleOutputModality m = createOutputModFromDef(output.size(),att,mmi);
			if(m!=null)output.add(m);	
		}	
		return output;
	}
	
	private ModalityAttrib loadAttributes(Element e){
		String packageName,className,interfaceName,type;
		boolean threaded, active;
		type = e.getAttribute("type");
		packageName = getValue("package",e);
		className = getValue("class",e);
		threaded = getBoolean("threaded",e);
		active = getBoolean("active",e);
		return new ModalityAttrib(type, threaded, packageName, className, active);
	}
	
	private Modality createInputModFromDef(int modID, ModalityAttrib modAttrib,MMInteractionManager mmi){
		String path = modAttrib.getPackageName()+"."+modAttrib.getClassName();
		Constructor<?> construct = null;
		try {
			try {
				construct = Class.forName(path).getConstructor(Integer.TYPE,ModalityAttrib.class,MMInteractionManager.class);
			} catch (NoSuchMethodException e1) {
				getLogger().error("Constructor not found");
				e1.printStackTrace();
			}
			try {
				try {
					Modality genericModality;
					if(modAttrib.isThreaded())genericModality = (ThreadedInputModality)construct.newInstance(modID,modAttrib,mmi);
					else genericModality = (Modality)construct.newInstance(modID,modAttrib,mmi);
					return genericModality;
				} catch (IllegalArgumentException
						| InvocationTargetException e) {
					getLogger().error("Couldn't instantiate constructor - Class: "+path);
					e.printStackTrace();
				}
			} catch (InstantiationException | IllegalAccessException e) {
				getLogger().error("Couldn't instantiate modality - Class: "+path);
				e.printStackTrace();
			}
		} catch (ClassNotFoundException e) {
			getLogger().error("The modality "+path+" wasn't found");
			e.printStackTrace();
		}
		return null;
	}
	private SimpleOutputModality createOutputModFromDef(int modID, ModalityAttrib modAttrib,MMInteractionManager mmi){
		String path = modAttrib.getPackageName()+"."+modAttrib.getClassName();
		Constructor<?> construct = null;
		try {
			try {
				construct = Class.forName(path).getConstructor(Integer.TYPE,ModalityAttrib.class,MMInteractionManager.class);
			} catch (NoSuchMethodException e1) {
				getLogger().error("Constructor not found");
				e1.printStackTrace();
			}
			try {
				try {
					SimpleOutputModality genericModality;
					if(modAttrib.isThreaded())genericModality = (ThreadedOutputModality)construct.newInstance(modID,modAttrib,mmi);
					else genericModality = (SimpleOutputModality)construct.newInstance(modID,modAttrib,mmi);
					return genericModality;
				} catch (IllegalArgumentException
						| InvocationTargetException e) {
					getLogger().error("Couldn't instantiate constructor - Class: "+path);
					e.printStackTrace();
				}
			} catch (InstantiationException | IllegalAccessException e) {
				getLogger().error("Couldn't instantiate modality - Class: "+path);
				e.printStackTrace();
			}
		} catch (ClassNotFoundException e) {
			getLogger().error("The modality "+path+" wasn't found");
			e.printStackTrace();
		}
		return null;
	}
	
	public void changeModalityActivationStatus(String tag, String modName,int modID, boolean activated){
		Element e = getElementList(tag).get(modID);
		Element f = e;
		if(activated)f.setAttribute("active", "true");
		else f.setAttribute("active", "false");
		overwriteAttribute(tag,modID,e,f);
		saveXMLChanges();
	}

	public void createNewRuntimeConfig(){
		//TODO: Load scheme, evaluates current settings and creates new xml file, which will be loaded
	}
	
}
