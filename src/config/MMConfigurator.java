package config;

import java.util.ArrayList;

import main.MMInteractionManager;
import modality.Modality;
import modality.Modality.response;
import modality.OutputInterface;
import modality.SimpleOutputModality;
import modality.ThreadedInputModality;
import modality.ThreadedOutputModality;
import util.Loggable;

public class MMConfigurator implements Loggable {
	
	private ArrayList<Modality> inputDevices;
	private ArrayList<SimpleOutputModality> outputDevices;
	private MMConfigParser mmparser;

	public ArrayList<Modality> getInputDevices() {
		return inputDevices;
	}
	
	public void setInputDevices(ArrayList<Modality> inputDevices) {
		this.inputDevices = inputDevices;
	}

	public ArrayList<SimpleOutputModality> getOutputDevices() {
		return outputDevices;
	}

	public void setOutputDevices(ArrayList<SimpleOutputModality> outputDevices) {
		this.outputDevices = outputDevices;
	}

	public void deativateOutput(String modName, int i){
		for(SimpleOutputModality output:outputDevices){
			if(output.getAttribute().getClassName()==modName){
				output.pause();
				mmparser.changeModalityActivationStatus(MMConfigParser.outputTag,modName,i,false);
			}
		}
	}
	public void activateOutput(String modName, int i){
		for(SimpleOutputModality output:outputDevices){
			if(output.getAttribute().getClassName()==modName){
				output.resume();
				mmparser.changeModalityActivationStatus(MMConfigParser.outputTag,modName,i,true);
			}
		}
	}
	public void deativateInput(String modName, int i){
		for(Modality input:inputDevices){
			if(input.getAttribute().getClassName()==modName){
				input.pause();
				mmparser.changeModalityActivationStatus(MMConfigParser.inputTag,modName,i,false);
			}
		}
	}
	public void activateInput(String modName, int i){
		for(Modality input:inputDevices){
			if(input.getAttribute().getClassName()==modName){
				input.resume();
				mmparser.changeModalityActivationStatus(MMConfigParser.inputTag,modName,i,true);
			}
		}
	}
	public MMConfigurator(MMInteractionManager mmi){
		mmparser = new MMConfigParser();
		getLogger().info("Loading modalities...");
		loadConfig(mmi);
		getLogger().info("Preparing active modalities...");
//		for(Modality m:inputDevices){
//			if(m.getAttribute().isActive()){
//				if(m.prepare()==response.SUCCESS) {
//					if(m.getAttribute().isThreaded()){
//						((ThreadedInputModality)m).startThread();
//					}
//				}else {
//					m.getAttribute().setActive(false);
//					m.cancel();
//				}
//			}	
//		}
//		
//		for(SimpleOutputModality m:outputDevices){
//			if(m.getAttribute().isActive()){
//				if(m.prepare()==response.SUCCESS) {
//					if(m.getAttribute().isThreaded()){
//						((ThreadedOutputModality)m).startThread();
//					}
//				}else {
//					m.getAttribute().setActive(false);
//					m.cancel();
//				}					
//			}	
//		}
	}
	
	private void loadConfig(MMInteractionManager mmi){
		inputDevices = mmparser.loadInputModalities(mmi);
		outputDevices = mmparser.loadOutputModalities(mmi);
	}
	
}
