package database;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import util.Loggable;

public class DBAdapter implements Loggable{
	protected DBConnection db;
	
	public DBAdapter(){
		db = DBConnection.getInstance();
	}
	
	protected ResultSet getResult(String query){
		ResultSet rs=null;
	    try {
	    	Statement statement = db.createStatement();
			rs = statement.executeQuery(query);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return rs;
	}
	
	protected void updateDB(String query){
		Statement statement = db.createStatement();
		if(statement!=null){
			try {
				statement.executeUpdate(query);
				getLogger().info(query);
				statement.close();
			} catch (SQLException e) {
				getLogger().error("Could not execute query");
				e.printStackTrace();
			}
		}
	}
	
	protected void execute(String query){
		Statement statement = db.createStatement();
		if(statement!=null){
			try {
				getLogger().info(query);
				statement.execute(query);
				statement.close();
			} catch (SQLException e) {
				getLogger().error("Could not execute query");
				e.printStackTrace();
			}
		}
	}
}
