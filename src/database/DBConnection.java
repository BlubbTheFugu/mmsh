package database;

import java.io.File;
import java.io.FileNotFoundException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Scanner;

import org.sqlite.SQLiteConfig;

import util.Loggable;

public class DBConnection implements Loggable{
	
	private static DBConnection instance;
	private Connection connection = null;
	private static SQLiteConfig config = null;
	private static final String DRIVER = "org.sqlite.JDBC";  
	private static final String dbName = "mmsh.db";
	
	private void createConnection() {  
	    try {
			Class.forName(DRIVER);
		} catch (ClassNotFoundException e) {
			getLogger().error("Couldn't find JDBC driver");
			e.printStackTrace();
		}
	    try {  
	        config = new SQLiteConfig();  
	        config.enforceForeignKeys(true);
	        connection = DriverManager.getConnection("jdbc:sqlite:"+dbName,config.toProperties());
	        Statement st = connection.createStatement();
	        st.execute("PRAGMA foreign_keys = true;");
	        st.close();
	    } catch (SQLException ex) {
	    	getLogger().error("Couldn't establish connection to db");
			ex.printStackTrace();
	    }    
	}
	
	public Connection getConnection() {
		return connection;
	}
	
	public Statement createStatement() {
		Statement s = null;
		try {
			s = connection.createStatement();
		} catch (SQLException e) {
			getLogger().error("Couldn't create db statement");
			e.printStackTrace();
		}
		return s;
	}

	private void loadSchemeFromFile(){
	    String delimiter = ";";
	    Scanner scanner;
	    ClassLoader classLoader = getClass().getClassLoader();
	    File inputFile = new File(classLoader.getResource("dbscheme.sql").getFile());
	    try {
	        scanner = new Scanner(inputFile).useDelimiter(delimiter);
	    } catch (FileNotFoundException e1) {
	        e1.printStackTrace();
	        return;
	    }
	    Statement currentStatement = null;
	    while(scanner.hasNext()) {
	        String rawStatement = scanner.next();
	        getLogger().info(rawStatement);
	        try {
	            currentStatement = connection.createStatement();
	            currentStatement.execute("PRAGMA foreign_keys = true;");
	            currentStatement.execute(rawStatement);
	        } catch (SQLException e) {
	        	getLogger().error("Couldn't execute sql: "+rawStatement);
	            e.printStackTrace();
	        } finally {
	            if (currentStatement != null) {
	                try {
	                    currentStatement.close();
	                } catch (SQLException e) {
	                	getLogger().error("Closing the db statement failed");
	                    e.printStackTrace();
	                }
	            }
	            currentStatement = null;
	        }
	    }
	    scanner.close();
	}
	
	private void initData(){
		Statement statement;
		getLogger().info("Fill with initial Data");
//		String[] roomNames={"Livingroom","Bathroom","Kitchen","Bedroom"};
		try {
			statement = connection.createStatement();
//		    for(int i=0;i<roomNames.length;i++){
//		    	statement.executeUpdate("INSERT INTO room VALUES ("+i+", '"+roomNames[i]+"', "+i+", "+i+", "+1+", "+1+", "+20+");");
//		    }


			statement.executeUpdate("INSERT INTO room VALUES (0, 'Bathroom', 63, 62, 80, 60, 18, 0, 1);");
			statement.executeUpdate("INSERT INTO room VALUES (1, 'Livingroom', 17, 125, 40, 60, 22, 0, 0);");
			statement.executeUpdate("INSERT INTO room VALUES (2, 'Kitchen', 194, 123, 100, 50, 20, 1, 1);");
			statement.executeUpdate("INSERT INTO room VALUES (3, 'Bedroom', 162, 42, 80, 80, 23, 0, 1);");
			statement.executeUpdate("INSERT INTO room VALUES (4, 'Floor', 121, 123, 60, 100, 25, 1, 0);");
		
			statement.execute("INSERT INTO playlist(playlist_id) VALUES (0);");
		    //Light sources
			statement.executeUpdate("INSERT INTO light_source VALUES (0, 0, 'asdf', 'Overhead lamp', 0);");
			statement.executeUpdate("INSERT INTO light_source VALUES (1, 0, 'asdf', 'Mirror light', 0);");
		    statement.executeUpdate("INSERT INTO light_source VALUES (2, 1, 'asdf', 'Overhead lamp', 1);");
		    statement.executeUpdate("INSERT INTO light_source VALUES (3, 1, 'asdf', 'Floor lamp', 0);");
		    statement.executeUpdate("INSERT INTO light_source VALUES (4, 1, 'asdf', 'Ambilight', 1);");
		    statement.executeUpdate("INSERT INTO light_source VALUES (5, 2, 'asdf', 'Overhead lamp', 1);");
		    statement.executeUpdate("INSERT INTO light_source VALUES (6, 2, 'asdf', 'Oven Light', 1);");
		    statement.executeUpdate("INSERT INTO light_source VALUES (7, 3, 'asdf', 'Overhead lamp', 1);");
		    statement.executeUpdate("INSERT INTO light_source VALUES (8, 3, 'asdf', 'Lava lamp', 0);");
		    statement.executeUpdate("INSERT INTO light_source VALUES (9, 3, 'asdf', 'Night stand', 0);");
		    statement.executeUpdate("INSERT INTO light_source VALUES (10, 3, 'asdf', 'Ambilight', 0);");
		    statement.executeUpdate("INSERT INTO light_source VALUES (11, 4, 'asdf', 'Overhead lamp', 0);");
		    statement.executeUpdate("INSERT INTO light_source VALUES (12, 4, 'asdf', 'Floor lamp', 0);");
		    
		    statement.executeUpdate("INSERT INTO power_socket VALUES (0, 0, 'asdf','Washing maschine', 1);");
		    statement.executeUpdate("INSERT INTO power_socket VALUES (1, 0, 'asdf','Drying maschine', 1);");
		    statement.executeUpdate("INSERT INTO power_socket VALUES (2, 1, 'asdf','TV', 1);");
		    statement.executeUpdate("INSERT INTO power_socket VALUES (3, 1, 'asdf','PC', 0);");
		    statement.executeUpdate("INSERT INTO power_socket VALUES (4, 1, 'asdf','Surround System', 0);");
		    statement.executeUpdate("INSERT INTO power_socket VALUES (5, 1, 'asdf', 'Telephone', 1);");
		    statement.executeUpdate("INSERT INTO power_socket VALUES (6, 2, 'asdf', 'Dishwasher', 0);");
		    statement.executeUpdate("INSERT INTO power_socket VALUES (7, 2, 'asdf', 'Fridge', 1);");
		    statement.executeUpdate("INSERT INTO power_socket VALUES (8, 2, 'asdf', 'Exhaust hood', 0);");
		    statement.executeUpdate("INSERT INTO power_socket VALUES (9, 2, 'asdf', 'Oven', 0);");
		    statement.executeUpdate("INSERT INTO power_socket VALUES (10, 2, 'asdf', 'Microwave', 1);");
		    statement.executeUpdate("INSERT INTO power_socket VALUES (11, 3, 'asdf', 'TV', 0);");
		    statement.executeUpdate("INSERT INTO power_socket VALUES (12, 3, 'asdf', 'Workstation', 0);");
		    statement.executeUpdate("INSERT INTO power_socket VALUES (13, 4, 'asdf', 'Router', 1);");
		    statement.executeUpdate("INSERT INTO power_socket VALUES (14, 4, 'asdf', 'Telephone', 1);");

		    statement.executeUpdate("INSERT INTO security VALUES (0,'Garage', 0);");
		    statement.executeUpdate("INSERT INTO security VALUES (1,'Intrusion Alarm', 1);");
		    statement.executeUpdate("INSERT INTO security VALUES (2,'Surveilance', 0);");
		    statement.executeUpdate("INSERT INTO security VALUES (3,'Smoke Alarm', 0);");
		    
		    statement.close();
		} catch (SQLException e) {
			getLogger().error("SQL Exception - Maybe something wrong with the scheme");
			e.printStackTrace();
		}
	}

	private void logDB(){
		String log="";
		try {
			log+="DB Settings - Autocommit: "+connection.getAutoCommit();
			log+=" Readonly: "+connection.isReadOnly();
			log+=" Closed: "+connection.isClosed();
			//log+="\n"+connection.getSchema();
			getLogger().info(log);
		} catch (SQLException e) {
			getLogger().error("Couldn't get DB-information");
			e.printStackTrace();
		}
	}

	private DBConnection(){
		File file = new File (dbName);
		boolean dbPresent = file.exists();
		createConnection();
    	if(!dbPresent){
    		getLogger().info("Creating database");
		    loadSchemeFromFile();
		    initData();
		}
    	getLogger().info("Loading database");
    	logDB();
	}
	
	public static DBConnection getInstance(){
		if(DBConnection.instance == null){
			DBConnection.instance = new DBConnection();
		}
		return DBConnection.instance;
	}
}
