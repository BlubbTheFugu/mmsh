package modality;

import main.MMInteractionManager;
import event.MMCmd;

public abstract class SimpleOutputModality extends Modality implements OutputInterface{


	public SimpleOutputModality(int modID, ModalityAttrib attribute,MMInteractionManager mmi) {
		super(modID, attribute,mmi);
	}

	protected abstract void executeOutput(MMCmd event);
	
	public void output(MMCmd event){
		if(getCurrentState().equals(state.IDLE)){
		    setCurrentState(state.RUNNING);
		    executeOutput(event);
		}
		setCurrentState(state.IDLE);
	}
	
	@Override
	public response start() {
		setCurrentState(state.IDLE);
		return response.SUCCESS;
	}

	@Override
	public response pause() {
		setCurrentState(state.PAUSED);
		return response.SUCCESS;
	}

	@Override
	public response resume() {
		setCurrentState(state.IDLE);
		return response.SUCCESS;
	}
}
