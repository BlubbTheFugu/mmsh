package modality;

import main.MMInteractionManager;

public abstract class ThreadedInputModality extends Modality implements ThreadedModality{
	
	protected Thread executionThread;
	
	public ThreadedInputModality(int modID, ModalityAttrib attribute,MMInteractionManager mmi) {
		super(modID, attribute, mmi);
		this.prepareThread();
		if(this.getAttribute().isActive())this.startThread();
	}
	
	protected abstract void threadedMethod();
	
	@Override
	public void prepareThread() {
		setCurrentState(state.IDLE);
		executionThread = new Thread(new Runnable(){
			@Override
			public void run() {
				while(getCurrentState()!=state.INITIAL){
					if(getCurrentState()!=state.PAUSED){
						setCurrentState(state.RUNNING);
						ThreadedInputModality.this.threadedMethod();
					}
					try {
						setCurrentState(state.IDLE);
						Thread.sleep(20);
						setCurrentState(state.RUNNING);
					} catch (InterruptedException e) {
						getLogger().error("Threading error");
						e.printStackTrace();
					}
				}
			}
		});
		executionThread.setDaemon(true);
	}
	
	@Override
	public void startThread(){
		executionThread.start();
	}

	@Override
	public void cancelThread() {
		setCurrentState(state.INITIAL);
		executionThread.interrupt();
		executionThread = null;
	}

}
