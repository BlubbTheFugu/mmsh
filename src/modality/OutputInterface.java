package modality;

import modality.Modality.response;
import event.MMCmd;

public interface OutputInterface {
	public void output(MMCmd outputEvent);
}
