package modality;

import java.util.LinkedList;

import main.MMInteractionManager;
import modality.Modality.response;
import modality.Modality.state;
import event.MMCmd;

public abstract class ThreadedOutputModality extends SimpleOutputModality implements ThreadedModality{

	protected LinkedList <MMCmd> outputQueue;
	private Thread executionThread;
	
	public ThreadedOutputModality(int modID, ModalityAttrib attribute,
			MMInteractionManager mmi) {
		super(modID, attribute, mmi);
		outputQueue = new LinkedList<MMCmd>();
		this.prepareThread();
		if(this.getAttribute().isActive())this.startThread();
	}
	
	public void startThread(){
		executionThread.start();
	}
	
	@Override
	public void output(MMCmd event) {
		outputQueue.add(event);
	}

	public void prepareThread() {
		setCurrentState(state.IDLE);
		executionThread = new Thread(new Runnable(){
			@Override
			public void run() {
				while(getCurrentState()!=state.INITIAL){
					if(!outputQueue.isEmpty()&&getCurrentState()!=state.PAUSED){
						setCurrentState(state.RUNNING);
						executeOutput(outputQueue.pollFirst());
						setCurrentState(state.IDLE);
					}
					try {
						Thread.sleep(20);
					} catch (InterruptedException e) {
						getLogger().error("Threading error");
						e.printStackTrace();
					}
				}
			}
		});
		executionThread.setDaemon(true);
	}
	
	@Override
	public void cancelThread() {
		setCurrentState(state.INITIAL);
		executionThread.interrupt();
		executionThread = null;
	}

}
