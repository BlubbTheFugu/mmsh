package modality;

import main.MMInteractionManager;
import util.Loggable;

public abstract class Modality implements Loggable{
	
	public enum state {
		INITIAL, IDLE, RUNNING, PAUSED
	}
	
	public enum response {
		SUCCESS, INVALID, FAILED
	}
	
	protected MMInteractionManager mmi;
	
	protected int modID;

	private state previousState;
	
	private state currentState;
	
	protected ModalityAttrib attribute;
	
	public abstract response prepare();
	public abstract response cancel();
	public abstract response start();
	public abstract response pause();
	public abstract response resume();
	
	public ModalityAttrib getAttribute() {
		return attribute;
	}

	public void setAttribute(ModalityAttrib attribute) {
		this.attribute = attribute;
	}

	public state getPreviousState() {
		return previousState;
	}

	public int getModID() {
		return modID;
	}

	public state getCurrentState() {
		return currentState;
	}
	
//	public void activate(){
//		this.setCurrentState(state.IDLE);
//		this.prepare();
//		this.getAttribute().setActive(true);
//	}
//	
//	public void deactivate(){
//		this.setCurrentState(state.INITIAL);
//		this.getAttribute().setActive(false);
//		this.cancel();
//	}

	public void setCurrentState(state currentState) {
		this.previousState = this.currentState;
		this.currentState = currentState;
	}
	
	public Modality(int modID, ModalityAttrib attribute,MMInteractionManager mmi) {
		this.mmi = mmi;
		this.modID = modID;
		this.attribute = attribute;
		setCurrentState(state.INITIAL);
		setCurrentState(state.IDLE);
		this.prepare();
	}
	
}
