package modality;

public class ModalityAttrib {
	
	private String type;
	private final boolean threaded;
	private String packageName;
	private String className;
	private boolean active;
	
	public boolean isActive() {
		return active;
	}
	public void setActive(boolean active) {
		this.active = active;
	}
	public String getPackageName() {
		return packageName;
	}
	public void setPackageName(String packageName) {
		this.packageName = packageName;
	}
	public String getClassName() {
		return className;
	}
	public void setClassName(String className) {
		this.className = className;
	}
	public boolean isThreaded() {
		return threaded;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	
	public ModalityAttrib(String type,boolean threaded,
			String packageName, String className, boolean active) {
		super();
		this.threaded = threaded;
		this.packageName = packageName;
		this.className = className;
		this.active = active;
	}
}
