package modality;

public interface ThreadedModality {
	
	public void startThread();
	public void prepareThread();
	public void cancelThread();
}
